﻿using ClosedXML.Excel;
using Com.Monty.Omni.Global.Business.Engines;
using Common;
using Common.Business.Common;
using Core.Common.Configurations;
using Dapper;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Monty.Omni.Framework.Business.Contracts.Engines;
using Monty.Omni.Framework.Entities;
using Npgsql;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text.RegularExpressions;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Common.Configuration;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;
using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Business.Engine
{
    public class MessageTemplateEngine : EngineBase, IMessageTemplateEngine
    {
        #region Fields
        private readonly IHttpClientAdapter _httpClientAdapter;
        private readonly IMessageTemplateRepository _messageTemplateRepository;
        private readonly IDateTimeEngine _dateTimeEngine;
        private readonly ITemplateRepository _templateRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine _brokerEngine;
        #endregion Fields

        #region Constructor
        public MessageTemplateEngine(IMessageTemplateRepository messageTemplateRepository,
            Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine brokerEngine,
            IDateTimeEngine dateTimeEngine, IHttpClientAdapter httpClientAdapter, ITemplateRepository templateRepository,
            IGroupRepository groupRepository,
            IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _messageTemplateRepository = messageTemplateRepository;
            _httpClientAdapter = httpClientAdapter;
            _dateTimeEngine = dateTimeEngine;
            _brokerEngine = brokerEngine;
            _templateRepository = templateRepository;
            _groupRepository = groupRepository;

        }

        #endregion Constructor

        #region Public Methods
        public List<WhatsappCampaignMessageSummary> Get(MessageTemplateRequestInfo messageTemplateRequestInfo)

        {


            WhatsappMessageTemplateOperation whatsappMessageTemplateOperation = new WhatsappMessageTemplateOperation
            {
                PageIndex = messageTemplateRequestInfo.GetPageIndex(),
                PageSize = messageTemplateRequestInfo.GetPageSize(),
                AccountId = messageTemplateRequestInfo.AccountId
            };



             _messageTemplateRepository.Get(whatsappMessageTemplateOperation);

            List<WhatsappCampaignMessageSummary> whatsappMessageTemplateSummaries = whatsappMessageTemplateOperation.Data.ToList();


            return whatsappMessageTemplateSummaries;

        }
        public ValidationMessageTemplateResponseInfo Save(MessageTemplateRequestSave messageTemplateRequestSave)
        {
            ValidationMessageTemplateResponseInfo validationMessageTemplateResponseInfo = new ValidationMessageTemplateResponseInfo();

            validationMessageTemplateResponseInfo = Validate(messageTemplateRequestSave.componentRequestInfo, messageTemplateRequestSave.TemplateName, messageTemplateRequestSave.AccountId);
            if(validationMessageTemplateResponseInfo.ValidData.Count > 0)
            {

           
            int campaignId = 0;
            if (messageTemplateRequestSave.IsImmediate)
            {
                messageTemplateRequestSave.State = Com.Monty.Omni.Global.Common.Enums.Enumerations.CampaignState.Running;
                messageTemplateRequestSave.ScheduleDate = DateTime.UtcNow;

            }
            else
            {
                messageTemplateRequestSave.State = CampaignState.Scheduled;

                if (messageTemplateRequestSave.ScheduleDate != null)
                {
                    DateTime dateConvert = new DateTime();
                    dateConvert = ParseStringDate(_dateTimeEngine.getDateByTimeZoneId(messageTemplateRequestSave.TimeZoneId, messageTemplateRequestSave.ScheduleDate.ToString()));

                    messageTemplateRequestSave.ScheduleDate = dateConvert;
                }
            }

                campaignId =   _messageTemplateRepository.Save(messageTemplateRequestSave);
            if(campaignId > 0)
            {
                _messageTemplateRepository.SaveDetail(messageTemplateRequestSave.componentRequestInfo, campaignId);
            }

            WhatsappMessageTemplate whatsappMessageTemplate = _messageTemplateRepository.GetById(campaignId, messageTemplateRequestSave.AccountId);


            _brokerEngine.PublishCampaignWhatsapp(whatsappMessageTemplate);
         //   validationMessageTemplateResponseInfo.IsValid = true;
            }
            return validationMessageTemplateResponseInfo;
        }
        public  int SaveList(MessageTemplateListRequestSave messageTemplateListRequestSave)
        {

            int campaignId = 0;
            if (messageTemplateListRequestSave.IsImmediate)
            {
                messageTemplateListRequestSave.State = Com.Monty.Omni.Global.Common.Enums.Enumerations.CampaignState.Running;
                messageTemplateListRequestSave.ScheduleDate = DateTime.UtcNow;

            }
            else
            {
                messageTemplateListRequestSave.State = CampaignState.Scheduled;

                if (messageTemplateListRequestSave.ScheduleDate != null)
                {
                    DateTime dateConvert = new DateTime();
                    dateConvert = ParseStringDate(_dateTimeEngine.getDateByTimeZoneId(messageTemplateListRequestSave.TimeZoneId, messageTemplateListRequestSave.ScheduleDate.ToString()));

                    messageTemplateListRequestSave.ScheduleDate = dateConvert;
                }
            }

            MessageTemplateRequestSave messageTemplateRequestSave = new MessageTemplateRequestSave();
           
            messageTemplateRequestSave.CampaignName = messageTemplateListRequestSave.CampaignName;
            messageTemplateRequestSave.TemplateName = messageTemplateListRequestSave.TemplateName;
            messageTemplateRequestSave.Language = messageTemplateListRequestSave.Language;
            messageTemplateRequestSave.ScheduleDate = messageTemplateListRequestSave.ScheduleDate;
            messageTemplateRequestSave.AccountId = messageTemplateListRequestSave.AccountId;
            messageTemplateRequestSave.State = messageTemplateListRequestSave.State;
            messageTemplateRequestSave.TimeZoneId = messageTemplateListRequestSave.TimeZoneId;
            messageTemplateRequestSave.TenantId = messageTemplateListRequestSave.TenantId;

            campaignId = _messageTemplateRepository.Save(messageTemplateRequestSave);
            if (campaignId > 0)
            {
                messageTemplateListRequestSave.componentRequestInfo.ForEach(c => c.campaign_message_template_id = campaignId);

                MergeComponentData<ComponentRequestInfo>(messageTemplateListRequestSave.componentRequestInfo, "whatsapp_campaign_message_template_detail");
                // _messageTemplateRepository.SaveDetail(messageTemplateListRequestSave.componentRequestInfo, campaignId);

                WhatsappMessageTemplate whatsappMessageTemplate = _messageTemplateRepository.GetById(campaignId, messageTemplateRequestSave.AccountId);
                _brokerEngine.PublishCampaignWhatsapp(whatsappMessageTemplate);
            }



            return 1;
        }
        public ValidationMessageTemplateResponseInfo ValidateList(List<ComponentRequestInfo> componentRequestInfo, string TemplateName,int accountId)
        {
           ValidationMessageTemplateResponseInfo validationMessageTemplateResponseInfo = new ValidationMessageTemplateResponseInfo();

            ValidationMessageTemplateResponseInfo validationMessageResponse = new ValidationMessageTemplateResponseInfo();

            foreach (var item in componentRequestInfo)
            {
                validationMessageResponse = Validate(item, TemplateName, accountId);

                if (validationMessageResponse.ValidData.Count > 0)
                {
                    validationMessageTemplateResponseInfo.Add(validationMessageResponse.ValidData, true);
                }
                else
                {
                    validationMessageTemplateResponseInfo.Add(validationMessageResponse.InValidData, false);
                }
           
            }


            return validationMessageTemplateResponseInfo;
        }
        public void Route(WhatsappMessageTemplate whatsappMessageTemplate)
        {
            try
            {
                //  _brokerEngine.PublishReport(campaign.TenantId, campaign.CampaignId, "", Action.Input, entity: campaign, microService: MicroService.CampaignRouting);
               
                if (IsScheduled(whatsappMessageTemplate.ScheduleDate))
                {
                    Schedule(whatsappMessageTemplate);
                }
                else
                {
                    Immediate(whatsappMessageTemplate);
                }
                // _brokerEngine.PublishReport(campaign.TenantId, campaign.CampaignId, "", Action.Processed, entity: campaign, microService: MicroService.CampaignRouting);
            }
            catch (Exception ex)
            {
                //  _brokerEngine.PublishReport(campaign.TenantId, campaign.CampaignId, "", Action.Failed, appError: ex.ToString(), microService: MicroService.CampaignRouting);
                logger.WriteLog(string.Format("{0} Error while Routing campaign Whatsapp message template CampaignId: {1} ", whatsappMessageTemplate.TenantId, whatsappMessageTemplate.CampaignId), ex);
            }

        }
        public async Task Immediate(WhatsappMessageTemplate whatsappMessageTemplate)
        {
            // ChangeCampaignState(pushNotificationMessage, CampaignState.InProgress);

            logger.WriteDebugLog(string.Format("Tenant {0} Campaign Message Template {1} Started.", whatsappMessageTemplate.TenantId, whatsappMessageTemplate.CampaignId));

            List<WhatsappMessageTemplateDetail> whatsappMessageTemplateDetailList = _messageTemplateRepository.GetWhatsappMessageTemplateDetailByCampaignId(whatsappMessageTemplate.CampaignId);

            UpdateMessageCampaignState(whatsappMessageTemplate.CampaignId, CampaignState.InProgress);
            foreach (var whatsappMessageTemplateDetail in whatsappMessageTemplateDetailList)
            {

            
            List<string> ListBodyString = _jsonAdapter.Deserialize<List<string>>(whatsappMessageTemplateDetail.BodyText);
            List<ParameterBody> listBody = new List<ParameterBody>();

            List<object> component = new List<object>();
          
            if(ListBodyString != null)
            {
                Body body = new Body();
                foreach (var item in ListBodyString)
                {
                    ParameterBody parameterBody = new ParameterBody();
                    parameterBody.type = "text";
                    parameterBody.text = item;
                    listBody.Add(parameterBody);
                }
                body.type = "body";
                body.parameters = listBody;

                component.Add(body);
            }
            List<ParameterHeaderText> listHeaderText = new List<ParameterHeaderText>();
            List<object> listHeaderMedia = new List<object>();
            if (whatsappMessageTemplateDetail.TypeHeader.ToLower() == "text" && !string.IsNullOrEmpty(whatsappMessageTemplateDetail.HeaderText))
            {
                ParameterHeaderText parameterHeader = new ParameterHeaderText();
                parameterHeader.type = "text";
                parameterHeader.text = whatsappMessageTemplateDetail.HeaderText;
                listHeaderText.Add(parameterHeader);
            }

            if (whatsappMessageTemplateDetail.TypeHeader.ToLower() == "media")
            {
                //object o = new object();
                //Media media = new Media();
               if(whatsappMessageTemplateDetail.MediaType.ToLower() == "image")
                {
                    object image = new
                    {
                        link = whatsappMessageTemplateDetail.Link,
                      //  filename = whatsappMessageTemplateDetail.HeaderName
                    };
                    object imageHeader = new
                    {
                        type = "image",
                        image = image
                    };

                    listHeaderMedia.Add(imageHeader);
                }

                if (whatsappMessageTemplateDetail.MediaType.ToLower() == "video")
                {
                    object video = new
                    {
                        link = whatsappMessageTemplateDetail.Link,
                     //   filename = whatsappMessageTemplateDetail.HeaderName
                    };
                    object videoHeader = new
                    {
                        type = "video",
                        video = video
                    };

                    listHeaderMedia.Add(videoHeader);
                }

                if (whatsappMessageTemplateDetail.MediaType.ToLower() == "document")
                {
                    object document = new
                    {
                        link = whatsappMessageTemplateDetail.Link,
                        filename = whatsappMessageTemplateDetail.FileName
                    };
                    object documentHeader = new
                    {
                        type = "document",
                        document = document
                    };

                    listHeaderMedia.Add(documentHeader);
                }

                //ParameterHeaderMedia parameterHeader = new ParameterHeaderMedia();
                //parameterHeader.type = "image";
                //media.link = whatsappMessageTemplateDetail.Link;
                //parameterHeader.image = media;

              //  listHeaderMedia.Add(parameterHeader);
            }
            if (listHeaderText.Count > 0)
            {
                HeaderText headerText = new HeaderText();
                headerText.type = "header";
                headerText.parameters = listHeaderText;
                component.Add(headerText);
            }
            
            if(listHeaderMedia.Count > 0)
            {
                HeaderMedia headerMedia = new HeaderMedia();
                headerMedia.type = "header";
                headerMedia.parameters = listHeaderMedia;
                component.Add(headerMedia);
            }

            List<object> list = new List<object>();

            WhatsappMessageTemplateBody whatsappMessageTemplateBody = new WhatsappMessageTemplateBody();
            Template template = new Template();
            Language language = new Language();
            
            language.code = whatsappMessageTemplate.Language;         
            template.name = whatsappMessageTemplate.TemplateName;
            template.language = language;
            template.components = component;
            whatsappMessageTemplateBody.messaging_product = whatsappMessageTemplate.MessagingProduct;
            whatsappMessageTemplateBody.recipient_type = whatsappMessageTemplate.RecipientType;
            whatsappMessageTemplateBody.to = whatsappMessageTemplateDetail.DestinationAddress;
            whatsappMessageTemplateBody.type = "template";
            whatsappMessageTemplateBody.template  = template;
            // list.Add(whatsappMessageTemplateBody);

            string Components = _jsonAdapter.Serialize(whatsappMessageTemplateBody);


            WhatsappAccount whatsappAccount = _messageTemplateRepository.GetWhatsappAccount(whatsappMessageTemplate.TenantId, whatsappMessageTemplate.AccountId);

            var task = SendCampaignTemplate(Components, whatsappAccount);
            task.Wait();

            var result = task.Result;

            if (result.StatusCode == "OK")
            {

                object cr = _jsonAdapter.Deserialize<object>(result.Message.ToString());


                var dataSuccess = _jsonAdapter.Deserialize<SuccessData>(cr.ToString());
                var id = dataSuccess.messages[0].id;
                    // UpdateMessageResponseId(whatsappMessageTemplate.CampaignId, id,CampaignState.Finished);

                    UpdateCampaignMessageTemplateResponseId(whatsappMessageTemplateDetail.Id, id);      
            }
            else
            {

                object cr = _jsonAdapter.Deserialize<object>(result.Message.ToString());

                var dataError = _jsonAdapter.Deserialize<ErrorsMessage>(cr.ToString());
  
                var errorList = _jsonAdapter.Deserialize<ErrorList>(dataError.Error.ToString());
                
               // var errorData = _jsonAdapter.Deserialize<ErrorData>(errorList.message.ToString());

                UpdateCampaignMessageTemplateErrorResponse(whatsappMessageTemplateDetail.Id, errorList.message) ;
            }
                UpdateMessageCampaignState(whatsappMessageTemplate.CampaignId, CampaignState.Finished);

            }
        }
        public IActionResult GenerateTemplateExcelFile(ExcelFileRequestInfo excelFileRequestInfo)
        {


            List<Contacts> contacts = _groupRepository.GetGroupDetail(excelFileRequestInfo.GroupId);
            List<TemplateParameter> templateParameters = _templateRepository.GetTemplateDetail(excelFileRequestInfo.AccountId, excelFileRequestInfo.TemplateName);

            int numberOfVariable;

            using (MemoryStream stream = new MemoryStream())
            {
                IXLWorkbook workbook = new XLWorkbook();
                IXLWorksheet worksheet = workbook.Worksheets.Add("Sample Sheet");
                //worksheet.Cell(1, 1).Value = "Name";
                worksheet.Cell(1, 1).Value = "Phone Number";
                if (contacts != null && contacts.Count > 0)
                {
                    for (int i = 0; i < contacts?.Count; i++)
                    {
                        worksheet.Cell(i + 2, 1).Value = contacts[i].MobileNo;

                    }
                }

                //for (int i = 0; i < contacts?.Count; i++)
                //{
                //    worksheet.Cell(i + 2, 1).Value = contacts[i].contact_name;
                //    worksheet.Cell(i + 2, 2).Value = contacts[i].mobile_number;
                //}
                int realcolumn = 1;
               // int position = 3 > 0 ? 3 : 0;

                int position = 0;
                //for (int i = 0; i < 3; i++)
                //{
                //    worksheet.Cell(1, realcolumn + i).Value = "param" + i;
                //}

                foreach (TemplateParameter templateParameter in templateParameters)
                {

                    if (templateParameter.typeRequest == "HEADER")
                    {

                        numberOfVariable = templateParameter.numberOfVariable;
                        if (numberOfVariable > 0)
                        {
                            for (int i = 1; i <= numberOfVariable; i++)
                            {
                                worksheet.Cell(1, realcolumn + 1).Value = "headerparam";
                            }
                            position = 1;
                        }
                        else if (templateParameter.format != "TEXT")
                        {

                            worksheet.Cell(1, realcolumn + 1).Value = "File Name";
                            worksheet.Cell(1, realcolumn + 2).Value = "File Link";
                            position = 2;
                        }
                    }
                    if (templateParameter.typeRequest == "BODY")
                    {
                        numberOfVariable = templateParameter.numberOfVariable;
                        if (numberOfVariable > 0)
                        {
                            // realcolumn = numberOfVariable;
                            //  position = numberOfVariable;

                            for (int i = 1; i <= numberOfVariable; i++)
                            {
                                worksheet.Cell(1, realcolumn + position + i).Value = "bodyparam" + i;
                            }
                        }
                    }
                }
                workbook.SaveAs(stream);
                return new FileContentResult(stream.ToArray(), "application/vnd.ms-excel") { FileDownloadName = "Sample.xls" };
            }
        }

        #endregion Public Methods

        #region Private Methods
        private bool IsScheduled(DateTime scheduledDate)
        {
            return (scheduledDate > DateTime.UtcNow.AddMinutes(1));
        }
        private void Schedule(WhatsappMessageTemplate whatsappMessageTemplate)
        {
            try
            {

                var expiry = whatsappMessageTemplate.ScheduleDate.Subtract(DateTime.UtcNow);
                string jobId = BackgroundJob.Schedule(() => ExecuteScheduleJob(whatsappMessageTemplate), expiry);

                _messageTemplateRepository.AssignJob(whatsappMessageTemplate.CampaignId, jobId);
                ChangeCampaignState(whatsappMessageTemplate, CampaignState.Scheduled);
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex, string.Format("{0} Error while Scheduling Whatsapp Template CampaignId: {1} ", whatsappMessageTemplate.TenantId, whatsappMessageTemplate.CampaignId));
                ChangeCampaignState(whatsappMessageTemplate, CampaignState.Failed);
            }

        }
        public Task ExecuteScheduleJob(WhatsappMessageTemplate whatsappMessageTemplate)
        {

            _brokerEngine.PublishCampaignWhatsapp(whatsappMessageTemplate);
            return Task.CompletedTask;
        }
        private void ChangeCampaignState(WhatsappMessageTemplate whatsappMessageTemplate, CampaignState campaignState)
        {
            try
            {
                _messageTemplateRepository.ChangeStatus(whatsappMessageTemplate.CampaignId, campaignState);
            }
            catch (Exception ex)
            {
                logger.WriteLog("Error in ChangeCampaignState Whatsappp Message Template", ex);

            }

        }
        private void UpdateMessageResponse(int campaignId, string message, CampaignState campaignState)
        {
            try
            {
                _messageTemplateRepository.UpdateMessage(campaignId, message, campaignState);
            }
            catch (Exception ex)
            {
                logger.WriteLog("Error in update message result campaign Whatsappp  Template", ex);

            }

        }

        private void UpdateMessageCampaignState(int campaignId,  CampaignState campaignState)
        {
            try
            {
                _messageTemplateRepository.UpdateMessageCampaignState(campaignId, campaignState);
            }
            catch (Exception ex)
            {
                logger.WriteLog("Error in update message result campaign Whatsappp  Template", ex);

            }

        }


        private void UpdateCampaignMessageTemplateResponseId(int messageTemplateDetailId, string WaId)
        {
            try
            {
                _messageTemplateRepository.UpdateMessageTemplateDetailWaId(messageTemplateDetailId, WaId);
            }
            catch (Exception ex)
            {
                logger.WriteLog("Error in update Wa_id campaign Whatsappp  Template", ex);

            }

        }

        private void UpdateCampaignMessageTemplateErrorResponse(int messageTemplateDetailId, string ErrorResponse)
        {
            try
            {
                _messageTemplateRepository.UpdateMessageTemplateDetailErrorResponse(messageTemplateDetailId, ErrorResponse);
            }
            catch (Exception ex)
            {
                logger.WriteLog("Error in update error result campaign Whatsappp  Template", ex);

            }

        }
        private static async Task<MessageTemplateResponseInfo> SendCampaignTemplate(string campaignTemplate, WhatsappAccount whatsappAccount)
        {
            MessageTemplateResponseInfo messageTemplateResponseInfo = new MessageTemplateResponseInfo();

            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
            string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessage");

            string addWhatsappTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.PhoneNumberId}{WhatsappMessageTemplate}";



            string authorizationKey = string.Format("Bearer {0}", whatsappAccount.Token);
            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();

            
            var http = new HttpClient();

            http.DefaultRequestHeaders.TryAddWithoutValidation(RequestHeaders.Authorization, authorizationKey);

           
            http.DefaultRequestHeaders.Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));


            StringContent queryString = new StringContent(campaignTemplate);

            StringContent httpContent = new StringContent(campaignTemplate, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.PostAsync(addWhatsappTemplateUrl, httpContent);

            string responseBody = await response.Content.ReadAsStringAsync();

            messageTemplateResponseInfo.StatusCode = response.StatusCode.ToString();
            messageTemplateResponseInfo.Message = responseBody;
            // templateResponseInfo.StatusCode = response.StatusCode.ToString();
            //  templateResponseInfo.Message = responseBody;

            //  var data = _jsonAdapter.Deserialize<PagingResponse>(cr.Data.ToString()).data.ToList();

            return messageTemplateResponseInfo;
        }
        private DateTime ParseStringDate(string stringDate)
        {
            string[] formats = { "yyyy-MM-dd HH:mm:ss" };
            DateTime dateParse = new DateTime();

            DateTime.TryParseExact(stringDate, formats, new CultureInfo("en-US"), DateTimeStyles.None,
               out dateParse);

            return dateParse;

        }
        private ValidationMessageTemplateResponseInfo Validate(ComponentRequestInfo componentRequestInfo, string TemplateName,int accountId)
        {
            ValidationMessageTemplateResponseInfo validationMessageTemplateResponseInfo = new ValidationMessageTemplateResponseInfo();

            List<TemplateParameter> templateParameters = _templateRepository.GetTemplateDetail(accountId, TemplateName);

            int numberOfVariable = 0;
            bool isValid = true;

            foreach (TemplateParameter templateParameter in templateParameters)
            {
                if (templateParameter.typeRequest == "HEADER")
                {

                    numberOfVariable = templateParameter.numberOfVariable;
                    if(string.IsNullOrEmpty(componentRequestInfo.media_type))
                    {
                        componentRequestInfo.Reason = "Media Type is required";
                        validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                        isValid = false;
                    }
                   else  if (templateParameter.format == "TEXT")
                    {
                        if (!string.IsNullOrEmpty(componentRequestInfo.type_header) &&  componentRequestInfo.type_header.ToLower() != "text")
                        {
                            componentRequestInfo.Reason = "Header type is invalid";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }
                       else if (string.IsNullOrEmpty(componentRequestInfo.header_text) && templateParameter.numberOfVariable == 1)
                        {
                            componentRequestInfo.Reason = "Header param is required";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }
                       
                    }
                    else if (templateParameter.format != "TEXT")
                    {
                        if (string.IsNullOrEmpty(componentRequestInfo.link))
                        {
                            componentRequestInfo.Reason = "link is required";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }

                       else if (templateParameter.format.ToLower() != componentRequestInfo.media_type.ToLower())
                        {

                            componentRequestInfo.Reason = "the channel media type is invalid";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }
                    }
                }
                if (isValid)
                {
                    if (templateParameter.typeRequest == "BODY")
                    {
                        numberOfVariable = templateParameter.numberOfVariable;
                        if (componentRequestInfo.body_text != null && numberOfVariable != componentRequestInfo.body_text.Count)
                        {
                            componentRequestInfo.Reason = "number of variable body is invalid ";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }

                        if (componentRequestInfo.body_text != null && componentRequestInfo.body_text.Any(i => i == null))
                        {
                            componentRequestInfo.Reason = "variable body could not contain empty";
                            validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
                            isValid = false;
                        }
                    }

                }
            }

            if (isValid)
            {

            if (!isNumber(componentRequestInfo.destination_address))
            {
                componentRequestInfo.Reason = "Phone Number should be integer";
                validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);
            }
          
            else if (componentRequestInfo.destination_address.Length <8 || componentRequestInfo.destination_address.Length>14)
            {
                componentRequestInfo.Reason = "Phone Number should be between 8 and 14 digits";
                validationMessageTemplateResponseInfo.Add(componentRequestInfo, false);

                //validationMessageTemplateResponseInfo.IsValid = false;
                //   validationMessageTemplateResponseInfo.Message = "Phone Number should be between 8 and 14 digits";
            }

            else
            {
                componentRequestInfo.Reason = "Sucess";
                validationMessageTemplateResponseInfo.Add(componentRequestInfo, true);
            }
            }
            return validationMessageTemplateResponseInfo;
        }
        private void MergeComponentData<T>(IEnumerable<T> objs, string destinationTable = "") where T : BaseEntities
        {

            try
            {
                string primarykey = "";

                primarykey = "generated_code";

                IEnumerable<PropertyInfo> props = objs.FirstOrDefault().GetType().GetProperties();

                string fieldsNames = "";
                var propertyFeilds = objs.FirstOrDefault().GetType().GetProperties();
                foreach (var item in propertyFeilds)
                {
                    var isVirtual = typeof(T).GetProperty(item.Name).GetGetMethod().IsVirtual;

                    if(isVirtual == false)
                    {
                        fieldsNames += item.Name.ToLower() + ",";
                    }
                    //   var name = item.GetCustomAttribute<ColumnAttribute>().Name ?? item.Name;

                    // updateQuery += name + " =EXCLUDED." + name + ",";
                  


                }


                string fields = fieldsNames.Trim(',');
                string create_temp = $"CREATE TEMP TABLE temp_{destinationTable} ON COMMIT DROP AS SELECT {fields} FROM {ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.{destinationTable} LIMIT 0";

                string copy = $"COPY temp_{destinationTable} ({fields})  FROM STDIN(FORMAT BINARY)";
                string upsert = $@"INSERT INTO {ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.{destinationTable}  ({fields})  (SELECT {fields} FROM temp_{destinationTable}) ; ";

                using (NpgsqlConnection connection = new NpgsqlConnection(Environment.GetEnvironmentVariable("WhatsappDbConnection")))
                {
                    connection.Open();
                    using (NpgsqlTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            List<string> cacheKeysList = new List<string>();
                            connection.Execute(create_temp);
                            using (NpgsqlBinaryImporter writer = connection.BeginBinaryImport(copy))
                            {
                                foreach (var obj in objs)
                                {
                                    //obj.TenantId = Convert.ToInt32(tenantid);
                                    writer.StartRow();
                                    var objFeilds = obj.GetType().GetProperties();
                                    foreach (var objFeild in objFeilds)
                                    {
                                        var isVirtual = typeof(T).GetProperty(objFeild.Name).GetGetMethod().IsVirtual;
                                        if (isVirtual == false)
                                        {

                                            var FeildType = objFeild.PropertyType;
                                            string GuidNullableCheck = "";
                                            string objtypeName;
                                            if (GuidNullableCheck == "")
                                            {
                                                objtypeName = objFeild.PropertyType.Name.ToString();
                                            }
                                            else
                                            {
                                                objtypeName = GuidNullableCheck;
                                            }
                                            object objectValue;
                                            DBTypeMappEnums FeildTypeEnum;

                                            if (objtypeName =="List`1")
                                            {
                                                FeildTypeEnum = DBTypeMappEnums.String;
                                                objectValue = objFeild.GetValue(obj);
                                                objectValue = _jsonAdapter.Serialize(objectValue);
                                            }

                                          
                                           else if (!Enum.TryParse(objtypeName, out FeildTypeEnum))
                                            {
                                                FeildTypeEnum = DBTypeMappEnums.Int32;
                                                objectValue = (int)objFeild.GetValue(obj);
                                            }
                                            else
                                            {
                                                FeildTypeEnum = (DBTypeMappEnums)Enum.Parse(typeof(DBTypeMappEnums), objtypeName);
                                                objectValue = objFeild.GetValue(obj);
                                            }
                                            NpgsqlTypes.NpgsqlDbType ValueAsEnum = (NpgsqlTypes.NpgsqlDbType)((int)FeildTypeEnum);

                                            writer.Write(objectValue, ValueAsEnum);

                                        }
                                    }

                                }
                                writer.Complete();
                            }
                            connection.Execute(upsert);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                    connection.Dispose();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private bool isNumber(string destinationAddress)
        {
            Regex regex = new Regex(@"[0-9]");
       

            if (regex.IsMatch(destinationAddress))
            {
                return true;
            }
            else return false;
        }

        #endregion Private Methods
    }
}
