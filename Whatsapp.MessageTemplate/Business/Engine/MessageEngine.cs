﻿using Com.Monty.Omni.Global.Business.Engines;
using Common;
using Common.Business.Common;
using Microsoft.AspNetCore.SignalR;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Hubs;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using static Whatsapp.MessageTemplate.Common.Enums.Enums;
using contacts = Whatsapp.MessageTemplate.Entities.contacts;
using messages = Whatsapp.MessageTemplate.Entities.messages;

namespace Whatsapp.MessageTemplate.Business.Engine
{
    public class MessageEngine : EngineBase, IMessageEngine
    {

        private readonly IHttpClientAdapter _httpClientAdapter;
        private readonly Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine _brokerEngine;
        private readonly IMessageTemplateRepository _messageTemplateRepository;
        private readonly IMessageRepository _messageRepository;
        //  private readonly Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine _brokerEngine;

        protected readonly IHubContext<ChatHub> _messageHub;

        private readonly IMessageValidationEngine _messageValidationEngine;
        public MessageEngine(Com.Monty.Omni.Global.Broker.Contracts.Engines.IBrokerEngine brokerEngine,
            IHttpClientAdapter httpClientAdapter, IServiceProvider serviceProvider, IMessageTemplateRepository messageTemplateRepository,
            IMessageRepository messageRepository, IMessageValidationEngine messageValidationEngine, IHubContext<ChatHub> messageHub) : base(serviceProvider)
        {
            _httpClientAdapter = httpClientAdapter;
            _brokerEngine = brokerEngine;
            _messageTemplateRepository = messageTemplateRepository;
            _messageRepository = messageRepository;
            _messageValidationEngine = messageValidationEngine;
            _messageHub = messageHub;
        }
        public ValidationMessageResponseInfo Save(MessageRequestSave messageRequestSave)
        {
            
            ValidationMessageResponseInfo validationMessageResponseInfo = Validate(messageRequestSave);

            if (validationMessageResponseInfo.IsValid)
            {

                var objects = new object();

                if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.text)
                {
                    WhatappMessageBodyText whatappMessageBody = new WhatappMessageBodyText();
                    whatappMessageBody.messaging_product = "whatsapp";
                    whatappMessageBody.recipient_type = "individual";
                    whatappMessageBody.to = messageRequestSave.DestinationAddress;
                    whatappMessageBody.type = messageRequestSave.messageType.ToString();

                    object text = new
                    {
                        body = messageRequestSave.MessageText,
                    };

                    whatappMessageBody.preview_url = messageRequestSave.preview_url;
                    whatappMessageBody.text = text;

                    objects = whatappMessageBody;
                }

                if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.image)
                {
                    WhatappMessageBodyImage whatappMessageBodyImage = new WhatappMessageBodyImage();
                    whatappMessageBodyImage.messaging_product = "whatsapp";
                    whatappMessageBodyImage.recipient_type = "individual";
                    whatappMessageBodyImage.to = messageRequestSave.DestinationAddress;
                    whatappMessageBodyImage.type = messageRequestSave.messageType.ToString();

                    media m = new media();
                    m.link = messageRequestSave.Link;
                    m.caption = messageRequestSave.Caption;
                    whatappMessageBodyImage.image = m;
                    objects = whatappMessageBodyImage;
                }

                if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.document)
                {
                    WhatappMessageBodyDocument whatappMessageBodyDocument = new WhatappMessageBodyDocument();
                    whatappMessageBodyDocument.messaging_product = "whatsapp";
                    whatappMessageBodyDocument.recipient_type = "individual";
                    whatappMessageBodyDocument.to = messageRequestSave.DestinationAddress;
                    whatappMessageBodyDocument.type = messageRequestSave.messageType.ToString();

                    media m = new media();
                    m.link = messageRequestSave.Link;
                    m.caption = messageRequestSave.Caption;
                    whatappMessageBodyDocument.document = m;
                    objects = whatappMessageBodyDocument;
                }

                if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.video)
                {
                    WhatappMessageBodyVideo whatappMessageBodyVideo = new WhatappMessageBodyVideo();
                    whatappMessageBodyVideo.messaging_product = "whatsapp";
                    whatappMessageBodyVideo.recipient_type = "individual";
                    whatappMessageBodyVideo.to = messageRequestSave.DestinationAddress;
                    whatappMessageBodyVideo.type = messageRequestSave.messageType.ToString();

                    media m = new media();
                    m.link = messageRequestSave.Link;
                    m.caption = messageRequestSave.Caption;
                    whatappMessageBodyVideo.video = m;

                    objects = whatappMessageBodyVideo;
                }

                string Components = _jsonAdapter.Serialize(objects);
                WhatsappAccount whatsappAccount = _messageTemplateRepository.GetWhatsappAccount(messageRequestSave.TenantId, messageRequestSave.AccountId);

                var task = SendMessage(Components, whatsappAccount);

                task.Wait();
                var result = task.Result;


                if (result.StatusCode == "OK")
                {
                    MessageResponseInfo messageResponseInfo = _jsonAdapter.Deserialize<MessageResponseInfo>(result.Message.ToString());
                    int resultSave =   _messageRepository.Save(messageRequestSave, messageResponseInfo);
                }

                else
                {
                    ErrorLiveChatMessage messageError = _jsonAdapter.Deserialize<ErrorLiveChatMessage>(result.Message);


                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = messageError.error.message;

                }

            }
            return validationMessageResponseInfo;
        }


       public int MessageRead(MessageMarkReadRequestSave messageMarkReadRequestSave)
        {

            WhatsappMessageRead whatsappMessageRead = new WhatsappMessageRead()
            {
                MessagingProduct = "whatsapp",
                Status = messageMarkReadRequestSave.Status.ToString(),
                MessageId = messageMarkReadRequestSave.MessageId
            };
            string message = _jsonAdapter.Serialize(whatsappMessageRead);
            WhatsappAccount whatsappAccount = _messageTemplateRepository.GetWhatsappAccount(messageMarkReadRequestSave.TenantId, messageMarkReadRequestSave.AccountId);

            SendMessageRead(message, whatsappAccount);
            return 1;
        }

        public void ProcessMessageReceived(object obj)
        {
            object cr = _jsonAdapter.Deserialize<object>(obj.ToString());

            var Data = _jsonAdapter.Deserialize<objects>(cr.ToString());

            List<entry> entry = Data.entry;

            List<changes> changes = entry[0].changes;

            string field = changes[0].field;

            if (field == "messages")
            {


                value v = changes[0].value;
                string messaging_product = v.messaging_product;
                metadata metadata = v.metadata;

                List<contacts> contacts = v.contacts;
                List<messages> messages = v.messages;

                List<statuses> statuses = v.statuses;


                string DisplayPhoneNumber = metadata.display_phone_number;
                string PhoneNumberId = metadata.phone_number_id;

        

                if (messages != null)
                {
                    string WaId = contacts[0].wa_id;
                    string ProfileName = contacts[0].profile.name;

                    string FromNumber = messages[0].from;
                    string MessageId = messages[0].id;
                    string Timestamp = messages[0].timestamp;

                    WhatsappMessageReceived whatsappMessageReceived = new WhatsappMessageReceived();
                    whatsappMessageReceived.DisplayPhoneNumber = DisplayPhoneNumber;
                    whatsappMessageReceived.PhoneNumberId = PhoneNumberId;
                    whatsappMessageReceived.Timestamp = Timestamp;
                    whatsappMessageReceived.WaId = WaId;
                    whatsappMessageReceived.ProfileName = ProfileName;
                    whatsappMessageReceived.FromNumber = FromNumber;
                    whatsappMessageReceived.MessageId = MessageId;
                    whatsappMessageReceived.Type = messages[0].type;

                    DateTime ReceiveddateTime = ConvertTimestampToDatetime(long.Parse(Timestamp));
                    whatsappMessageReceived.ReceivedDate = ReceiveddateTime;
                    if (messages[0].type == MessageType.text)
                    {
                        whatsappMessageReceived.Body = messages[0].text.body;
                    }
                    if (messages[0].type == MessageType.document)
                    {
                        whatsappMessageReceived.Caption = messages[0].document.caption;
                        whatsappMessageReceived.FileName = messages[0].document.filename;
                        whatsappMessageReceived.MimeType = messages[0].document.mime_type;
                        whatsappMessageReceived.Sha256 = messages[0].document.sha256;
                        whatsappMessageReceived.MediaId = messages[0].document.id;

                    }
                    if (messages[0].type == MessageType.video)
                    {
                        whatsappMessageReceived.Caption = messages[0].video.caption;
                        whatsappMessageReceived.MimeType = messages[0].video.mime_type;
                        whatsappMessageReceived.Sha256 = messages[0].video.sha256;
                        whatsappMessageReceived.MediaId = messages[0].video.id;

                    }
                    if (messages[0].type == MessageType.image)
                    {
                        whatsappMessageReceived.Caption = messages[0].image.caption;
                        whatsappMessageReceived.MimeType = messages[0].image.mime_type;
                        whatsappMessageReceived.Sha256 = messages[0].image.sha256;
                        whatsappMessageReceived.MediaId = messages[0].image.id;

                    }
                    if (messages[0].type == MessageType.sticker)
                    {

                        whatsappMessageReceived.MimeType = messages[0].sticker.mime_type;
                        whatsappMessageReceived.Sha256 = messages[0].sticker.sha256;
                        whatsappMessageReceived.MediaId = messages[0].sticker.id;

                    }
                    if (messages[0].type == MessageType.audio)
                    {

                        whatsappMessageReceived.MimeType = messages[0].audio.mime_type;
                        whatsappMessageReceived.Sha256 = messages[0].audio.sha256;
                        whatsappMessageReceived.MediaId = messages[0].audio.id;
                        whatsappMessageReceived.Voice = messages[0].audio.voice;
                    }


                    _messageRepository.SaveMessageReceived(whatsappMessageReceived);

                  
                    _messageHub.Clients.All.SendAsync("ReceiveMessage", messages[0]);
                }

                if(statuses != null)
                {
                    WhatsappMessageReceivedStatus whatsappMessageReceivedStatus = new WhatsappMessageReceivedStatus()
                    {
                        WaId = statuses[0].Id,

                        Status = (WhatsappMessageStatus)Enum.Parse(typeof(WhatsappMessageStatus), statuses[0].Status),
                        RecipientId = statuses[0].RecipientId

                    };
                    _messageRepository.SaveMessageReceivedStatus(whatsappMessageReceivedStatus);

                    _messageHub.Clients.All.SendAsync("ReceiveMessage", whatsappMessageReceivedStatus);

                    
             
                }
            }
    

        }

       
        public  List<WhatsappMessage> Get(MessageRequestInfo messageRequestInfo)
        {

            WhatsappMessageOperation whatsappMessageOperation = new WhatsappMessageOperation
            {
                PageIndex = messageRequestInfo.GetPageIndex(),
                PageSize = messageRequestInfo.GetPageSize(),
                AccountId = messageRequestInfo.AccountId
            };


            _messageRepository.Get(whatsappMessageOperation);
            List<WhatsappMessage> whatsappMessages = whatsappMessageOperation.Data.ToList();



            return whatsappMessages;
        }

        public List<WhatsappMessage> GetByDestination(MessageRequestInfo messageRequestInfo)
        {

            WhatsappMessageOperation whatsappMessageOperation = new WhatsappMessageOperation
            {
                PageIndex = messageRequestInfo.GetPageIndex(),
                PageSize = messageRequestInfo.GetPageSize(),
                AccountId = messageRequestInfo.AccountId,
                DestinationAddress = messageRequestInfo.Destinationaddress
            };


            _messageRepository.GetByDestination(whatsappMessageOperation);
            List<WhatsappMessage> whatsappMessages = whatsappMessageOperation.Data.ToList();



            return whatsappMessages;
        }

        private static async Task<MessageTemplateResponseInfo> MessageMarkRead(string message, WhatsappAccount whatsappAccount)
        {
            MessageTemplateResponseInfo messageTemplateResponseInfo = new MessageTemplateResponseInfo();

            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
            string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessage");

            string addWhatsappTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.PhoneNumberId}{WhatsappMessageTemplate}";


            string authorizationKey = string.Format("Bearer {0}", whatsappAccount.Token);
            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();


            var http = new HttpClient();

            http.DefaultRequestHeaders.TryAddWithoutValidation(RequestHeaders.Authorization, authorizationKey);


            http.DefaultRequestHeaders.Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));


            StringContent queryString = new StringContent(message);

            StringContent httpContent = new StringContent(message, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.PostAsync(addWhatsappTemplateUrl, httpContent);

            string responseBody = await response.Content.ReadAsStringAsync();

            messageTemplateResponseInfo.StatusCode = response.StatusCode.ToString();
            messageTemplateResponseInfo.Message = responseBody;

            //  return 1;
            return messageTemplateResponseInfo;
        }

        private static async Task<MessageTemplateResponseInfo> SendMessage(string message, WhatsappAccount whatsappAccount)
        {
          MessageTemplateResponseInfo messageTemplateResponseInfo = new MessageTemplateResponseInfo();

            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
            string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessage");

            string addWhatsappTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.PhoneNumberId}{WhatsappMessageTemplate}";


            string authorizationKey = string.Format("Bearer {0}", whatsappAccount.Token);
            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();


            var http = new HttpClient();

            http.DefaultRequestHeaders.TryAddWithoutValidation(RequestHeaders.Authorization, authorizationKey);


            http.DefaultRequestHeaders.Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));


            StringContent queryString = new StringContent(message);

            StringContent httpContent = new StringContent(message, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.PostAsync(addWhatsappTemplateUrl, httpContent);

            string responseBody = await response.Content.ReadAsStringAsync();

            messageTemplateResponseInfo.StatusCode = response.StatusCode.ToString();
            messageTemplateResponseInfo.Message = responseBody;

          //  return 1;
          return messageTemplateResponseInfo;
        }
        private ValidationMessageResponseInfo Validate(MessageRequestSave messageRequestSave)
        {
            ValidationMessageResponseInfo validationMessageResponseInfo = new ValidationMessageResponseInfo();
           
            if(string.IsNullOrEmpty(messageRequestSave.DestinationAddress))
            {
                validationMessageResponseInfo.IsValid = false;
                validationMessageResponseInfo.Reason = "Destination Address is required";

            }

            if (!_messageValidationEngine.isNumber(messageRequestSave.DestinationAddress))
            {
                validationMessageResponseInfo.IsValid = false;
                validationMessageResponseInfo.Reason = "Destination Address  should be a number";

            }

            else  if (messageRequestSave.DestinationAddress.Length < 8 || messageRequestSave.DestinationAddress.Length >14)
            {
                validationMessageResponseInfo.IsValid = false;
                validationMessageResponseInfo.Reason = "Destination Address length should be between 8 and 14";

            }

          else  if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.text && string.IsNullOrEmpty(messageRequestSave.MessageText))
            {
                validationMessageResponseInfo.IsValid = false;
                validationMessageResponseInfo.Reason = "Message text is required";
            }

          else if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.image || messageRequestSave.messageType == Common.Enums.Enums.MessageType.video)
            {
                if (string.IsNullOrEmpty(messageRequestSave.Link))
                {
                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = "Link is required";

                }

                //if (!_messageValidationEngine.IsValidURL(messageRequestSave.Link))
                //{
                //    validationMessageResponseInfo.IsValid = false;
                //    validationMessageResponseInfo.Reason = "Link is Invalid";

                //}

                else if (string.IsNullOrEmpty(messageRequestSave.Caption))
                {
                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = "Caption is required";

                }

                else
                {
                    validationMessageResponseInfo.IsValid = true;
                    validationMessageResponseInfo.Reason = "Success";

                }

            }

          else  if (messageRequestSave.messageType == Common.Enums.Enums.MessageType.document )
            {
                if (string.IsNullOrEmpty(messageRequestSave.Link))
                {
                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = "Link is required";

                }

              else  if (string.IsNullOrEmpty(messageRequestSave.Caption))
                {
                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = "Caption is required";

                }

              else  if (string.IsNullOrEmpty(messageRequestSave.FileName))
                {
                    validationMessageResponseInfo.IsValid = false;
                    validationMessageResponseInfo.Reason = "file name is required";

                }
                else
                {
                    validationMessageResponseInfo.IsValid = true;
                    validationMessageResponseInfo.Reason = "Success";

                }


            }
          
          else
            {
                validationMessageResponseInfo.IsValid = true;
                validationMessageResponseInfo.Reason = "Success";

            }

            return validationMessageResponseInfo;
        }

        public int Webhook(object o)
        {
            object obj = _jsonAdapter.Deserialize<object>(o.ToString());
            _brokerEngine.PublishWhatsappMessageReceived(obj);
            return 1;
        }
        private DateTime ConvertTimestampToDatetime(long timestmp)
        {
            DateTime rslt = DateTimeOffset.FromUnixTimeSeconds(timestmp).DateTime;
            return rslt;
        }


        private static async Task<MessageTemplateResponseInfo> SendMessageRead(string message, WhatsappAccount whatsappAccount)
        {
            MessageTemplateResponseInfo messageTemplateResponseInfo = new MessageTemplateResponseInfo();

            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
            string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessage");

            string addWhatsappTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.PhoneNumberId}{WhatsappMessageTemplate}";


            string authorizationKey = string.Format("Bearer {0}", whatsappAccount.Token);
            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();


            var http = new HttpClient();

            http.DefaultRequestHeaders.TryAddWithoutValidation(RequestHeaders.Authorization, authorizationKey);


            http.DefaultRequestHeaders.Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));


            StringContent queryString = new StringContent(message);

            StringContent httpContent = new StringContent(message, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response = await http.PostAsync(addWhatsappTemplateUrl, httpContent);

            string responseBody = await response.Content.ReadAsStringAsync();

            messageTemplateResponseInfo.StatusCode = response.StatusCode.ToString();
            messageTemplateResponseInfo.Message = responseBody;

            //  return 1;
            return messageTemplateResponseInfo;
        }

    }
}
