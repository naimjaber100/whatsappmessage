﻿using System.Text.RegularExpressions;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;

namespace Whatsapp.MessageTemplate.Business.Helpers
{
    public class MessageValidationEngine : IMessageValidationEngine
    {

        public bool isNumber(string destinationAddress)
        {
            Regex regex = new Regex(@"[0-9]");


            if (regex.IsMatch(destinationAddress))
            {
                return true;
            }
            else return false;
        }


        public bool IsValidURL(string URL)
        {
            string Pattern = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";
            Regex Rgx = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return Rgx.IsMatch(URL);
        }
    }
}
