﻿using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using Whatsapp.MessageTemplate.Client.Requests;

namespace Whatsapp.MessageTemplate.Business.Contracts.Services
{
    public interface IMessageTemplateService
    {

        ClientResponse Get(MessageTemplateRequestInfo messageTemplateRequestInfo);
        ClientResponse Save(MessageTemplateRequestSave messageTemplateRequestInfo);

        ClientResponse SaveList(MessageTemplateListRequestSave messageTemplateListRequestSave);

        ClientResponse ValidateList(List<ComponentRequestInfo> componentRequestInfo,string TemplateName, int accountId);

        IActionResult GenerateTemplateExcelFile(ExcelFileRequestInfo excelFileRequestInfo);

    }
}
