﻿using Common.Client.Response;
using Whatsapp.MessageTemplate.Client.Requests;

namespace Whatsapp.MessageTemplate.Business.Contracts.Services
{
    public interface IMessageService
    {
        ClientResponse Save(MessageRequestSave messageRequestSave);


        ClientResponse MessageRead(MessageMarkReadRequestSave messageMarkReadRequestSave);

        ClientResponse Webhook(object o);

        ClientResponse Get(MessageRequestInfo messageRequestInfo);
        ClientResponse GetByDestination(MessageRequestInfo messageRequestInfo);
    }
}
