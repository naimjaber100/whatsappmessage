﻿using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Business.Contracts.Engines
{
    public interface IMessageEngine
    {
        ValidationMessageResponseInfo Save(MessageRequestSave messageRequestSave);

        int MessageRead(MessageMarkReadRequestSave messageMarkReadRequestSave);
        int Webhook(object o);

        void ProcessMessageReceived(object obj);

        List<WhatsappMessage> Get(MessageRequestInfo messageRequestInfo);

        List<WhatsappMessage> GetByDestination(MessageRequestInfo messageRequestInfo);


    }
}
