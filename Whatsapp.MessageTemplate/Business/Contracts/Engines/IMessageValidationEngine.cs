﻿namespace Whatsapp.MessageTemplate.Business.Contracts.Engines
{
    public interface IMessageValidationEngine
    {

        bool isNumber(string destinationAddress);
        bool IsValidURL(string URL);
    }
}
