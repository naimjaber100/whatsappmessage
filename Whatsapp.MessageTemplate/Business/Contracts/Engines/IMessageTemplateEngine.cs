﻿using Microsoft.AspNetCore.Mvc;
using Monty.Omni.Framework.Entities;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Business.Contracts.Engines
{
    public interface IMessageTemplateEngine
    {

        List<WhatsappCampaignMessageSummary> Get(MessageTemplateRequestInfo messageTemplateRequestInfo);
        ValidationMessageTemplateResponseInfo Save(MessageTemplateRequestSave messageTemplateRequestSave);

        int SaveList(MessageTemplateListRequestSave messageTemplateListRequestSave);

        ValidationMessageTemplateResponseInfo ValidateList(List<ComponentRequestInfo> componentRequestInfo, string TemplateName,int accountId);
        void Route(WhatsappMessageTemplate whatsappMessageTemplate);


        IActionResult GenerateTemplateExcelFile(ExcelFileRequestInfo excelFileRequestInfo);

    }
}
