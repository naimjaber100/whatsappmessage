﻿using ClosedXML.Excel;
using Common;
using Common.Business.Common;
using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Business.Services
{
    public class MessageTemplateService : CommonBase, IMessageTemplateService
    {
        private readonly IMessageTemplateEngine _messageTemplateEngine;
        public MessageTemplateService(ILogAdapter LogAdapter, IJsonAdapter JsonAdapter, IMessageTemplateEngine messageTemplateEngine) : base(LogAdapter, JsonAdapter)
        {
            _messageTemplateEngine = messageTemplateEngine;
        }


       public ClientResponse Get(MessageTemplateRequestInfo messageTemplateRequestInfo)
        {

               List<WhatsappCampaignMessageSummary> campaignMessagenEntities = _messageTemplateEngine.Get(messageTemplateRequestInfo);

            ////   List<PushNotificationAppResponseInfo> appResponseList = _autoMapper.Mapper.Map<List<PushNotificationAppSummary>, List<PushNotificationAppResponseInfo>>(pushNotificationEntities);

            PagingResponse pagingResponse = new PagingResponse()
            {
                totalRows = campaignMessagenEntities.Count == 0 ? 0 : campaignMessagenEntities.FirstOrDefault().TotalRows,
                data = campaignMessagenEntities
            };

            return Success(pagingResponse);
        }
        public ClientResponse Save(MessageTemplateRequestSave messageTemplateRequestSave)
        {
            ValidationMessageTemplateResponseInfo result = _messageTemplateEngine.Save(messageTemplateRequestSave);


            if (result.ValidData.Count > 0) return Success();
            else return NotFoundOperation(result.InValidData[0].Reason);
           // return result.IsValid == true ? Success() : BadOperation(result.Message);


        }


        public ClientResponse SaveList(MessageTemplateListRequestSave messageTemplateListRequestSave)
        {
          int x =  _messageTemplateEngine.SaveList(messageTemplateListRequestSave);
            return Success();
        }

       public ClientResponse ValidateList(List<ComponentRequestInfo> componentRequestInfo, string TemplateName, int accountId)
        {
            ValidationMessageTemplateResponseInfo ValidationMessageTemplateResponseInfo = _messageTemplateEngine.ValidateList(componentRequestInfo, TemplateName , accountId);
            return Success(new
            {
                valid = ValidationMessageTemplateResponseInfo.ValidData,
                inValid = ValidationMessageTemplateResponseInfo.InValidData
            });
        }
        public IActionResult GenerateTemplateExcelFile(ExcelFileRequestInfo excelFileRequestInfo)
        {
            return _messageTemplateEngine.GenerateTemplateExcelFile(excelFileRequestInfo);
        }
    }
}
