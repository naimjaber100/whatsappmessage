﻿using Common;
using Common.Business.Common;
using Common.Client.Response;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Business.Services
{
    public class MessageService : CommonBase, IMessageService
    {

        private IMessageEngine _messageEngine;
        public MessageService(ILogAdapter LogAdapter, IJsonAdapter JsonAdapter, IMessageEngine messageEngine) : base(LogAdapter, JsonAdapter)
        {
            _messageEngine = messageEngine;
        }
        public ClientResponse Save(MessageRequestSave messageRequestSave)
        {
            ValidationMessageResponseInfo validationMessageResponseInfo  = _messageEngine.Save(messageRequestSave);



            if (validationMessageResponseInfo.IsValid) return Success();

            else return NotFoundOperation(validationMessageResponseInfo.Reason);
     
        }

        public ClientResponse MessageRead(MessageMarkReadRequestSave messageMarkReadRequestSave)
        {

           int messageRead = _messageEngine.MessageRead(messageMarkReadRequestSave);


            return Success();
        }

       public ClientResponse Webhook(object o)
        {
            logAdapter.WriteInfoLog("whatsapp webhook Service");
            _messageEngine.Webhook(o);

            return Success("whatsapp webhook");
        }


       public ClientResponse Get(MessageRequestInfo messageRequestInfo)
        {


           List<WhatsappMessage> whatsappMessages = _messageEngine.Get(messageRequestInfo);


            PagingResponse pagingResponse = new PagingResponse()
            {
                totalRows = whatsappMessages.Count == 0 ? 0 : whatsappMessages.FirstOrDefault().TotalRows,
                data = whatsappMessages
            };

            return Success(pagingResponse);
        }

        public ClientResponse GetByDestination(MessageRequestInfo messageRequestInfo)
        {


            List<WhatsappMessage> whatsappMessages = _messageEngine.GetByDestination(messageRequestInfo);


            PagingResponse pagingResponse = new PagingResponse()
            {
                totalRows = whatsappMessages.Count == 0 ? 0 : whatsappMessages.FirstOrDefault().TotalRows,
                data = whatsappMessages
            };

            return Success(pagingResponse);
        }
    }
}
