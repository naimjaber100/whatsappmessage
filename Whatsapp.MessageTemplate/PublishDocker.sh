

echo "whatsappmessage --- STARTING"

#cd MTSmppServer/
docker rmi sms-repo.montymobile.com/whatsappmessage:1.0.0
rm -frd publish/
dotnet publish Whatsapp.MessageTemplate.csproj -c Release -o publish
docker build -t sms-repo.montymobile.com/whatsappmessage:1.0.0 .
docker push sms-repo.montymobile.com/whatsappmessage:1.0.0
rm -frd publish/
cd ..
cd ..

echo "whatsappmessage --- FINISHED"

sleep 1d