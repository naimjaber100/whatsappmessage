﻿using Com.Monty.Omni.Global.Business.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Web.Http;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Business.Hubs
{
    [AuthorizeAccountType(AccountType.Business, AccountType.SBusiness)]
    public class ChatHub : Hub<IChatHub>
    {

        public static ConcurrentDictionary<string, string> MyUsers = new ConcurrentDictionary<string, string>();
     
        public async Task SendMessage(string message)
        {
            await Clients.All.SendMessage("ReceiveMessage", message);


        }

        public async Task SendMessageById(string message,string connectionId)
        {
       //  var x=   GetConnectionId();
            await Clients.User(connectionId).SendMessageId("ReceiveMessage", message);


        }

        public class MyUserType
        {
            public string? ConnectionId { get; set; }
  
        }
        public string GetConnectionId(string AccountId)
        {

            // var identity = (ClaimsIdentity)Context.User.Identity;
            //   return Context.ConnectionId;
            string ConnectionId;
            MyUsers.TryGetValue(AccountId, out ConnectionId);
            return ConnectionId;
        }
    
        public override Task OnConnectedAsync()
        {
           // var accessToken = Context.UserIdentifier;
           // var accessToken2 = Context.GetHttpContext();
            string accessToken = Context.GetHttpContext().Request.Query["access_token"];
            // var identity = Context.User.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID")).Value;
            //Console.WriteLine(identity);

            var handler = new JwtSecurityTokenHandler();
           // string authHeader = Request.Headers["Authorization"];
           string  authHeader = accessToken.Replace("bearer ", "");
            var jsonToken = handler.ReadToken(authHeader);
            var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
            
            var id = tokenS.Claims.First(claim => claim.Type == "ExternalID").Value;

            MyUsers.TryAdd(id,  Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {

            string accessToken1 = Context.GetHttpContext().Request.Query["access_token"];

            var handler = new JwtSecurityTokenHandler();
            string authHeader = accessToken1.Replace("bearer ", "");
            var jsonToken = handler.ReadToken(authHeader);
            var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;

            var id = tokenS.Claims.First(claim => claim.Type == "ExternalID").Value;
            string value1;
            MyUsers.TryRemove(id,out value1);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
