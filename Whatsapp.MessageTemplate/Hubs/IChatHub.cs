﻿namespace Whatsapp.MessageTemplate.Business.Hubs
{
    public interface IChatHub
    {

      // void SendMessage(string message);

        Task SendMessage(string test,string message);
        Task SendMessageId(string test, string message);
    }
}
