﻿namespace Whatsapp.MessageTemplate.Client.Responses
{
    public class MessageTemplateResponseInfo
    {
        public string? StatusCode { get; set; }
        public string? Message { get; set; }
    }


    public class ErrorsMessage
    {
        public object? Error { get; set; }
    }

    public class ErrorList
    {
        public string? message { get; set; } = null;
        public string? type { get; set; }
        public object? error_data { get; set; }
    }

    public class ErrorData
    {
        public string? messaging_product { get; set; } = null;
        public string? details { get; set; }
    }

    public class SuccessData
    {
        public string? messaging_product { get; set; }
        public List<object>? contacts { get; set; }
        public List<messages>? messages { get; set; }
    }

    public class messages
    {
        public string? id { get; set; }
    }
}
