﻿using Whatsapp.MessageTemplate.Client.Requests;

namespace Whatsapp.MessageTemplate.Client.Responses
{
    public class ValidationMessageTemplateResponseInfo
    {
        #region Fields 

        public List<ComponentRequestInfo> ValidData;
        public List<ComponentRequestInfo> InValidData;
        #endregion Fields 



        #region Constructors
        public ValidationMessageTemplateResponseInfo()
        {
            ValidData = new List<ComponentRequestInfo>();
            InValidData = new List<ComponentRequestInfo>();
        }
        #endregion Constructors


        #region Public Methods

        public void Add(ComponentRequestInfo componentRequestInfo, bool isValid)
        {
            if (isValid)
                ValidData.Add(componentRequestInfo);
            if (!isValid)
                InValidData.Add(componentRequestInfo);
        }


        public void Add(List<ComponentRequestInfo> componentRequestInfos,bool isValid)
        {
            if (componentRequestInfos.Count == 0)
                return;

            if (isValid)
                ValidData.AddRange(componentRequestInfos);
            if (!isValid)
                InValidData.AddRange(componentRequestInfos);
        }
        #endregion Public Methods

        //public bool IsValid { get; set; }
        //public string? Message { get; set; }
    }
}
