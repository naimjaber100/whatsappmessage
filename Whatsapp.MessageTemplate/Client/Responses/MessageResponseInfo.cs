﻿namespace Whatsapp.MessageTemplate.Client.Responses
{
    public class MessageResponseInfo
    {
        public string? messaging_product { get; set; }

        public List<contacts>? contacts { get; set; }

        public List<messages>? messages { get; set; }
    }

    public class contacts
    {
        public string? input { get; set; }
        public string? wa_id { get; set; }
    }



    public class ErrorLiveChatMessage
    {
        public error? error { get; set; }
    }

    public class error
    {
        public string? message { get; set; }
        public string ? type { get; set; }
        
    }
  
}
