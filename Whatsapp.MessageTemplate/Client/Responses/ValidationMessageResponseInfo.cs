﻿namespace Whatsapp.MessageTemplate.Client.Responses
{
    public class ValidationMessageResponseInfo
    {

        public bool IsValid { get; set; }

        public string? Reason { get; set; }
    }
}
