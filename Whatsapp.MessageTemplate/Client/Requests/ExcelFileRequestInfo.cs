﻿namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class ExcelFileRequestInfo
    {
        public int AccountId { get;set; }
        public string? TemplateName { get;set; }

        public int GroupId { get;set; }

    }
}
