﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using Whatsapp.MessageTemplate.Entities;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageTemplateRequestSave:MessageTemplateHeaderRequestSave
    {
        public ComponentRequestInfo? componentRequestInfo { get; set; }


    }
}
