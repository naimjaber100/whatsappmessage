﻿using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageMarkReadRequestSave
    {
        public string? MessagingProduct {get;set;}
        public WhatsappMessageStatus? Status { get; set; }
        public string? MessageId { get; set; }
        public int AccountId { get; set; }
        public int TenantId { get; set; }
    }
}
