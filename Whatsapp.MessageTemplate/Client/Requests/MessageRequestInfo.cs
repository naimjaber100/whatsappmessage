﻿using Com.Monty.Omni.Global.Client.Entities.Requests;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageRequestInfo : BasePagingRequestInfo
    {

        public int AccountId { get; set; }

        public string? Destinationaddress { get; set; }
    }
}
