﻿using Com.Monty.Omni.Global.Client.Entities.Requests;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageTemplateRequestInfo : BasePagingRequestInfo
    {


        public int AccountId { get; set; }

        public string? CampaignName  { get; set; }
    }
}
