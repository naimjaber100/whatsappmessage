﻿using Microsoft.AspNetCore.Mvc;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class Hub
    {
        [FromQuery(Name = "hub.mode")]
        public string? mode { get; set; }

        [FromQuery(Name = "hub.challenge")]
        public int challenge { get; set; }

        [FromQuery(Name = "hub.verify_token")]
        public string? verify_token { get; set; }
    }
}
