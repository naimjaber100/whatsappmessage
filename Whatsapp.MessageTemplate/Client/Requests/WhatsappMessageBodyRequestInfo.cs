﻿namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class WhatsappMessageBodyRequestInfo
    {
        public string? messaging_product { get; set; }
        public string? recipient_type { get; set; }
        public string? to { get; set; }
        public string? type { get; set; }
    }
}
