﻿using System.ComponentModel.DataAnnotations;
using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageRequestSave
    {
        public int AccountId { get; set; }
        [Required]
        public string?  DestinationAddress { get; set; }
        [Required]
        public MessageType messageType { get; set; }
        public bool preview_url { get; set; }
        public string? MessageText { get; set; }
        public string? Link { get; set; }
        public string? Caption { get; set; }
        public string? FileName { get; set; }

        public int TenantId { get; set; }

    }
}
