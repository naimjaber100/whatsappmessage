﻿
using System.ComponentModel.DataAnnotations;
using Whatsapp.MessageTemplate.Entities;
using static Whatsapp.MessageTemplate.Common.Enums.Enums;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class ComponentRequestInfo:BaseEntities
    {
        [JsonProperty("text")]
        public List<string>? body_text { get; set; }
        [JsonProperty("typeHeader")]
        public string? type_header { get; set; }
        public string? link { get; set; }
        [JsonProperty("fileName")]
        public string? file_name { get; set; }
        [JsonProperty("headerText")]
        public string? header_text { get; set; }
        [JsonProperty("mediaType")]
        public string? media_type { get; set; }

        [Required]
        [JsonProperty("DestinationAddress")]
       
      //  [RegularExpression(@"^[0-9]+$", ErrorMessage = "destination address should only be number")]
        public string? destination_address { get; set; }
        
        public Int64 campaign_message_template_id { get; set; }

        public virtual string? Reason { get; set; }
    }
}
