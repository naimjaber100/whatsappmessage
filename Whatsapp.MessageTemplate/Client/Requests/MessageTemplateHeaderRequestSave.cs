﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Whatsapp.MessageTemplate.Entities;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Client.Requests
{
    public class MessageTemplateHeaderRequestSave
    {

        public int AccountId { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public int TenantId { get; set; }
        [Required(ErrorMessage = "Campaign Name is required")]
        [MaxLength(50, ErrorMessage = "Campaign Name cannot be longer than 50 characters.")]
        public string? CampaignName { get; set; }
        [Required]
        public string? Language { get; set; }

        //[Required]
        //public string ? FromPhoneNumber { get; set; }

        [Required(ErrorMessage = "Template Name is required")]
        public string? TemplateName { get; set; }

        public CampaignState State { get; set; }
        [Required]
        public bool IsImmediate { get; set; }
        public string? TimeZoneId { get; set; }


       
    }
}
