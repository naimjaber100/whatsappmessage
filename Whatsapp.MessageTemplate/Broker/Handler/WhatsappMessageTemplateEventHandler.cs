﻿using Brokers.EventBus.Abstractions;
using Com.Monty.Omni.Global.Broker.Contracts.Engines;
using Common.Business.Common;
using Monty.Omni.Framework.Broker.Events;
using Monty.Omni.Framework.Entities;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Broker.Handler
{
    public class WhatsappMessageTemplateEventHandler : IIntegrationEventHandler<Monty.Omni.Framework.Broker.Events.WhatsappMessageCampaignEvent>
    {

       // private IPushNotificationEngine _pushNotificationEngine;
        private ILogAdapter _logAdapter;

        private IBrokerEngine _brokerEngine;
        private readonly IMessageTemplateEngine _messageTemplateEngine;
        public WhatsappMessageTemplateEventHandler(IMessageTemplateEngine messageTemplateEngine, ILogAdapter logAdapter, IBrokerEngine brokerEngine)
        {
            _messageTemplateEngine = messageTemplateEngine;

            _logAdapter = logAdapter;
            _brokerEngine = brokerEngine;


        }

        public Task Handle(Monty.Omni.Framework.Broker.Events.WhatsappMessageCampaignEvent @event)
        {
            WhatsappMessageTemplate message = @event.WhatsappMessageTemplate;
            try
            {
               if (message != null)
                {
                    _messageTemplateEngine.Route(message);
            //        //   _brokerEngine.PublishReport(message.TenantId, message.CampaignId, message.MessageID, Action.Processed);
              }

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex, "WhatsappCampaignTemplateEventHandler => error");

            }
            return Task.CompletedTask;
        }

    }
}
