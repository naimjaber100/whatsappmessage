﻿using Brokers.EventBus.Abstractions;
using Com.Monty.Omni.Global.Broker.Contracts.Engines;
using Common.Business.Common;
using Microsoft.AspNetCore.SignalR;
using Monty.Omni.Framework.Broker.Events;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Hubs;

namespace Whatsapp.MessageTemplate.Broker.Handler
{
    public class WhatsappMessageReceivedEventHandler : IIntegrationEventHandler<Monty.Omni.Framework.Broker.Events.WhatsappMessageReceivedEvent>
    {
        private ILogAdapter _logAdapter;

        private IBrokerEngine _brokerEngine;
        private readonly IMessageEngine _messageTemplateEngine;
        protected readonly IHubContext<ChatHub> _messageHub;
        public WhatsappMessageReceivedEventHandler(IHubContext<ChatHub> messageHub, IMessageEngine messageTemplateEngine, ILogAdapter logAdapter, IBrokerEngine brokerEngine)
        {
            _messageTemplateEngine = messageTemplateEngine;

            _logAdapter = logAdapter;
            _brokerEngine = brokerEngine;
            _messageHub = messageHub;
        }

        public Task Handle(WhatsappMessageReceivedEvent @event)
        {
            object message = @event.obj;
            try
            {
                if (message != null)
                {
                    _messageTemplateEngine.ProcessMessageReceived(message);
                    _logAdapter.WriteInfoLog( String.Format("whatsapp message received",message));
                   
                   //  _messageHub.Clients.All.SendAsync("sendToReact", "The message '" +
              //  "' has been received");
                }

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex, "WhatsappMessageReceivedEventHandler => error");

            }
            return Task.CompletedTask;
        }
    }
}
