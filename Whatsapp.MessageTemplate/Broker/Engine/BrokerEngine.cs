﻿using Com.Monty.Omni.Global.Business.Contracts.Engines;
using Brokers.EventBus.Configurations;
using Com.Monty.Omni.Global.Business.Entities;
using Whatsapp.MessageTemplate.Entities;
using Com.Monty.Omni.Global.Common.Static;
using Whatsapp.MessageTemplate.Broker.Handler;
using Monty.Omni.Framework.Entities;
using Com.Monty.Omni.Global.Broker.Contracts.Engines;
using Monty.Omni.Framework.Broker.Events;

namespace Whatsapp.MessageTemplate.Broker.Engine
{
    public class BrokerEngine : Com.Monty.Omni.Global.Broker.Engines.BrokerEngine, IBrokerEngine
    {
        #region ctr 
        public BrokerEngine(IServiceProvider serviceProvider) : base(serviceProvider)
        {
       
        }
        #endregion ctr

        #region public functions
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            base.StartAsync(cancellationToken);
           Init();
            return Task.CompletedTask;
        }
     

        //public void PublishScheduleCampaignWhatsapp(WhatsappMessageTemplate whatsappMessageTemplate)
        //{
        //    ExchangeSetting exchangeSetting = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.WhatsappMessageTemplateExchangeSettings);
        //    ScheduledWhatsappCampaignEvent campaignEvent = new ScheduledWhatsappCampaignEvent()
        //    {
        //        WhatsappMessageTemplate = whatsappMessageTemplate
        //    };
        //    Tenant tenant = _tenantManager.Get(whatsappMessageTemplate.TenantId);
        //    PublishRegistration<ScheduledWhatsappCampaignEvent>(exchangeSetting, tenant.TenantKey);
        //    Publish(campaignEvent, tenant.TenantKey, exchangeSetting.QueueName);
        //}
    

     

        #endregion public functions


        private void Init(string virtualHost = null)
        {
            ExchangeSetting exchangeSettingWhatsappCampaignTemplate = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.WhatsappMessageTemplateExchangeSettings);
            Subscribe<WhatsappMessageCampaignEvent, WhatsappMessageTemplateEventHandler>(exchangeSettingWhatsappCampaignTemplate);

            ExchangeSetting exchangeSettingWhatsappMessageReceived = _configurationEngine.GetEventBusExchangeSetting(AppConfigs.OmniChannelSection, AppConfigs.WhatsappMessageReceivedExchangeSettings);
            Subscribe<WhatsappMessageReceivedEvent, WhatsappMessageReceivedEventHandler>(exchangeSettingWhatsappMessageReceived);


        }


    }
}
