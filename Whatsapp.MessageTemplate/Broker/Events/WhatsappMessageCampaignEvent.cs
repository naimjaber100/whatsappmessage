﻿using Brokers.EventBus.Events;
using Monty.Omni.Framework.Entities;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Broker.Events
{
    public class WhatsappMessageCampaignEvent : IntegrationEvent
    {
        public WhatsappMessageTemplate WhatsappMessageTemplate { get; set; }
    }
}
