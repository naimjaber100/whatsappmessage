﻿using Brokers.EventBus.Events;
using Monty.Omni.Framework.Entities;
using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Broker.Events
{
    public class ScheduledWhatsappCampaignEvent : IntegrationEvent
    {
        public WhatsappMessageTemplate WhatsappMessageTemplate { get; set; }
    }
}
