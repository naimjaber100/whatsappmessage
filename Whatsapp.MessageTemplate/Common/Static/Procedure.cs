﻿using Core.Common.Configurations;
using Whatsapp.MessageTemplate.Common.Configuration;

namespace Whatsapp.MessageTemplate.Common.Static
{
    public class Procedure
    {
        public static readonly string AddCampaignMessageTemplate = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_template_insert";
        public static readonly string AddCampaignMessageTemplateDetail = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_template_detail_insert";

        public static readonly string GetCampaignMessageTemplate = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_template_get";

        public static readonly string GetCampaignMessageTemplateDetail = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_template_detail_get";


        public static readonly string UpdateJobId = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_update_job";

        public static readonly string UpdateWhatsappCampaignState = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_update_state";

        public static readonly string UpdateWhatsappCampaignMessageResult = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_update_message_result";
       // public static readonly string UpdateWhatsappCampaignStateResult = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_update_state_result";


        public static readonly string UpdateWhatsappCampaignMessageDetailWaIdResult = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_detail_update_waid";
        public static readonly string UpdateWhatsappCampaignMessageDetailErrorResponseResult = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_detail_update_error_response";


        public static readonly string GetWhatsappAccount = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_account_get";

        public static readonly string GetTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_parameter_get_by_template_id";
        public static readonly string GetContactDetail = $"{ApplicationConfiguration.DbConnectionSchemaName}.udf_get_group_details";


        public static readonly string GetCampaignMessageTemplateAll = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_campaign_message_template_get_all";


        public static readonly string AddMessage = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_message_insert";


        public static readonly string AddMessageReceived = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_message_received_insert";


        public static readonly string UpdateMessageStatus = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_message_update_status";


        public static readonly string GetMessagesByAccountId = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_message_get_by_account_id";
        public static readonly string GetMessagesByDestinationAddress = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_message_get_by_destination_address";

    }
}
