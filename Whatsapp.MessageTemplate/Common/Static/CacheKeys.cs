﻿namespace Whatsapp.MessageTemplate.Common.Static
{
    public class CacheKeys
    {
        public static readonly string WhatsappAccountKey = "WhatsappAccount" + "-{0}" + "-{1}";
    }
}
