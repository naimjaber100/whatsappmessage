﻿namespace Whatsapp.MessageTemplate.Common.Enums
{
    public class Enums
    {
        public enum TemplateTypeBody
        {
            HEADER = 1,
            BODY = 2
       
        }
        public enum HeaderType
        {
            text = 1,
            Media = 2

        }

        public enum MediaTemplateType
        {
        
            image = 1,
            video = 2,
            document = 3,
           

        }
        public enum MessageType
        {
            text = 1,
            image = 2,
            video = 3,
            document = 4,
            audio = 5,
            sticker =6,

        }

        public enum WhatsappMessageStatus
        {
            sent = 1,
            delivered = 2,
            read = 3,
            failed = 4
           

        }
    }
}
