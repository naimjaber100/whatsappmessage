﻿using Common.Business.Common;
using Common.Enums;
using Dapper;
using Whatsapp.MessageTemplate.Common.Static;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;

namespace Whatsapp.MessageTemplate.Persistence.Data.Repositories
{

    public class TemplateRepository : WhatsappRepositoryBase<dynamic>, ITemplateRepository
    {
        private readonly IJsonAdapter _jsonAdapter;
        private readonly ICacheAdapter _cacheAdapter;
        private ILogAdapter _logAdapter;
        public TemplateRepository(ILogAdapter logAdapter, Func<CacheType, ICacheAdapter> CacheAdapter, IJsonAdapter jsonAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
            _jsonAdapter = jsonAdapter;
            _logAdapter = logAdapter;
        }

        public List<TemplateParameter> GetTemplateDetail(int accountId, string templateName)
        {

            DynamicParameters dynamicParameters = new DynamicParameters();

            dynamicParameters.Add("_account_id", accountId);
            dynamicParameters.Add("_template_name", templateName);
            List<TemplateParameter> templateParameter = GetList<TemplateParameter>(Procedure.GetTemplateParameter, dynamicParameters).ToList();


            return templateParameter;
        }
    }
}
