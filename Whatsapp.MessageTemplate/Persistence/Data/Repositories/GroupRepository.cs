﻿using Com.Monty.Omni.Global.Global.Data.Repositories;
using Common.Business.Common;
using Common.Enums;
using Dapper;
using Whatsapp.MessageTemplate.Common.Static;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;

namespace Whatsapp.MessageTemplate.Persistence.Data.Repositories
{
    public class GroupRepository : RepositoryBase<dynamic>, IGroupRepository
    {
        private readonly IJsonAdapter _jsonAdapter;
        private readonly ICacheAdapter _cacheAdapter;
        private ILogAdapter _logAdapter;

        public GroupRepository(ILogAdapter logAdapter, Func<CacheType, ICacheAdapter> CacheAdapter, IJsonAdapter jsonAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
            _jsonAdapter = jsonAdapter;
            _logAdapter = logAdapter;
        }

        public List<Contacts> GetGroupDetail(int groupId)
        {

            DynamicParameters dynamicParameters = new DynamicParameters();

            dynamicParameters.Add("p_groupid", groupId);
            List<Contacts> contacts = GetList<Contacts>(Procedure.GetContactDetail, dynamicParameters).ToList();


            return contacts;
        }
    }
}
