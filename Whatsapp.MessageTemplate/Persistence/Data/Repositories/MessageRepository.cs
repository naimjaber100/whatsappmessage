﻿using Common.Business.Common;
using Common.Enums;
using Dapper;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Common.Static;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;

namespace Whatsapp.MessageTemplate.Persistence.Data.Repositories
{
    public class MessageRepository : WhatsappRepositoryBase<dynamic>, IMessageRepository
    {
        private readonly IJsonAdapter _jsonAdapter;
        private readonly ICacheAdapter _cacheAdapter;
        private ILogAdapter _logAdapter;
        public MessageRepository(ILogAdapter logAdapter, Func<CacheType, ICacheAdapter> CacheAdapter, IJsonAdapter jsonAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
            _jsonAdapter = jsonAdapter;
            _logAdapter = logAdapter;
        }

        public int Save(MessageRequestSave messageRequestSave, MessageResponseInfo messageResponseInfo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_messaging_product", "whatsapp");
            parameters.Add("_recipient_type", "individual");
            parameters.Add("_destination_address", messageRequestSave.DestinationAddress);
            parameters.Add("_type",(int) messageRequestSave.messageType);
            parameters.Add("_preview_url", messageRequestSave.preview_url);
            parameters.Add("_text", messageRequestSave.MessageText);
            parameters.Add("_link", messageRequestSave.Link);
            parameters.Add("_caption", messageRequestSave.Caption);
            parameters.Add("_account_id", messageRequestSave.AccountId);
            parameters.Add("_tenant_id", messageRequestSave.TenantId);
            parameters.Add("_wa_id", messageResponseInfo.messages[0].id);
            var id = GetSingle<int>(Procedure.AddMessage, parameters);
            return (int)id;
        }


        public int SaveMessageReceived(WhatsappMessageReceived whatsappMessageReceived)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_phone_number_id", whatsappMessageReceived.PhoneNumberId);
            parameters.Add("_display_phone_number", whatsappMessageReceived.DisplayPhoneNumber);
            parameters.Add("_profile_name", whatsappMessageReceived.ProfileName);
            parameters.Add("_wa_id", whatsappMessageReceived.WaId);
            parameters.Add("_from_number", whatsappMessageReceived.FromNumber);
            parameters.Add("_message_id", whatsappMessageReceived.MessageId);
            parameters.Add("_timestamp", whatsappMessageReceived.Timestamp);
            parameters.Add("_message_date", whatsappMessageReceived.ReceivedDate);
            parameters.Add("_type",(int) whatsappMessageReceived.Type);
            parameters.Add("_caption", whatsappMessageReceived.Caption);
            parameters.Add("_mime_type", whatsappMessageReceived.MimeType);
            parameters.Add("_sha256", whatsappMessageReceived.Sha256);
            parameters.Add("_media_id", whatsappMessageReceived.MediaId);
            parameters.Add("_field", whatsappMessageReceived.Field);
            parameters.Add("_body", whatsappMessageReceived.Body);
            parameters.Add("_voice", whatsappMessageReceived.Voice);
            parameters.Add("_filename", whatsappMessageReceived.FileName);
            var id = GetSingle<int>(Procedure.AddMessageReceived, parameters);
            return (int)id;

        }


        public int SaveMessageReceivedStatus(WhatsappMessageReceivedStatus whatsappMessageReceivedStatus)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_wa_id", whatsappMessageReceivedStatus.WaId);
            parameters.Add("_status",(int)whatsappMessageReceivedStatus.Status);
            parameters.Add("_recipient_id", whatsappMessageReceivedStatus.RecipientId);
          
            var id = GetSingle<int>(Procedure.UpdateMessageStatus, parameters);
            return (int)id;
        }

       public void Get(WhatsappMessageOperation whatsappMessageOperation)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_page_index", whatsappMessageOperation.PageIndex);
            parameters.Add("_page_size", whatsappMessageOperation.PageSize);
            parameters.Add("_account_id", whatsappMessageOperation.AccountId);

            whatsappMessageOperation.Data = GetList<WhatsappMessage>(Procedure.GetMessagesByAccountId, parameters).ToList();
    
        }

        public void GetByDestination(WhatsappMessageOperation whatsappMessageOperation)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_page_index", whatsappMessageOperation.PageIndex);
            parameters.Add("_page_size", whatsappMessageOperation.PageSize);
            parameters.Add("_account_id", whatsappMessageOperation.AccountId);
            parameters.Add("_destination_address", whatsappMessageOperation.DestinationAddress);

            whatsappMessageOperation.Data = GetList<WhatsappMessage>(Procedure.GetMessagesByDestinationAddress, parameters).ToList();

        }
    }
}
