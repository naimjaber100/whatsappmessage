﻿using Common.Business.Common;
using Common.Enums;
using Common.Extensions;
using Dapper;
using Monty.Omni.Framework.Entities;
using System.Data;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Common.Static;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Persistence.Data.Repositories
{
    public class MessageTemplateRepository : WhatsappRepositoryBase<dynamic>, IMessageTemplateRepository
    {
        private readonly IJsonAdapter _jsonAdapter;
        private readonly ICacheAdapter _cacheAdapter;
        private ILogAdapter _logAdapter;
        public MessageTemplateRepository(ILogAdapter logAdapter,Func<CacheType, ICacheAdapter> CacheAdapter, IJsonAdapter jsonAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
            _jsonAdapter = jsonAdapter;
            _logAdapter = logAdapter;
        }

        public void Get(WhatsappMessageTemplateOperation whatsappMessageTemplateOperation)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_page_index", whatsappMessageTemplateOperation.PageIndex);
            dynamicParameters.Add("_page_size", whatsappMessageTemplateOperation.PageSize);
            dynamicParameters.Add("_account_id", whatsappMessageTemplateOperation.AccountId);


            whatsappMessageTemplateOperation.Data = GetList<WhatsappCampaignMessageSummary>(Procedure.GetCampaignMessageTemplateAll, dynamicParameters);

          //  var result = GetList<WhatsappCampaignMessage>(Procedure.GetCampaignMessageTemplateAll, dynamicParameters).ToList();
           // return result;
        }
        public WhatsappMessageTemplate GetById(int campaignId, int accountId)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_campaign_id", campaignId);
            dynamicParameters.Add("_account_id", accountId);


            var result = GetSingle<WhatsappMessageTemplate>(Procedure.GetCampaignMessageTemplate, dynamicParameters);
            return result;
        }

       public List<WhatsappMessageTemplateDetail> GetWhatsappMessageTemplateDetailByCampaignId(int campaignId)
        {
          
            DynamicParameters dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("_campaign_id", campaignId);

            return  GetList<WhatsappMessageTemplateDetail>(Procedure.GetCampaignMessageTemplateDetail, dynamicParameters).ToList();


        }
        public int Save(MessageTemplateRequestSave messageTemplateRequestSave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_campaign_name", messageTemplateRequestSave.CampaignName);
            parameters.Add("_messaging_product", "whatsapp");
            parameters.Add("_recipient_type", "individual");
            parameters.Add("_template_name", messageTemplateRequestSave.TemplateName);
            parameters.Add("_language", messageTemplateRequestSave.Language);
            parameters.Add("_schedule_date", messageTemplateRequestSave.ScheduleDate);
            parameters.Add("_account_id", messageTemplateRequestSave.AccountId);
            parameters.Add("_state",(int) messageTemplateRequestSave.State);
            parameters.Add("_timezone_id", messageTemplateRequestSave.TimeZoneId);
            parameters.Add("_tenant_id", messageTemplateRequestSave.TenantId);
            parameters.Add("_type", "template");

            var id = GetSingle<int>(Procedure.AddCampaignMessageTemplate, parameters);
            return (int)id;
        }

        public int SaveDetail(ComponentRequestInfo componentRequestInfo,int campaignMessageId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_campaign_meesage_template_id", campaignMessageId);
            parameters.Add("_type_header", componentRequestInfo.type_header);
            parameters.Add("_link", componentRequestInfo.link);
            parameters.Add("_file_name", componentRequestInfo.file_name);
            parameters.Add("_type_body", "text");
            parameters.Add("_body_text", _jsonAdapter.Serialize(componentRequestInfo.body_text));
            parameters.Add("_destination_address", componentRequestInfo.destination_address);
            parameters.Add("_header_text", componentRequestInfo.header_text);
            parameters.Add("_media_type", componentRequestInfo.media_type);

            var id = GetSingle<int>(Procedure.AddCampaignMessageTemplateDetail, parameters);
            return (int)id;
        }


        public void AssignJob(int campaignId, string jobId)
        {
           
                var parameters = new DynamicParameters();
                parameters.Add("p_id", campaignId, DbType.Int64);
                parameters.Add("p_job_id", jobId);
                ExecuteScalar(Procedure.UpdateJobId, parameters: parameters);
           
        }


        public void ChangeStatus(int campaignId, CampaignState campaignState)
        {
            try
            {

                var parameters = new DynamicParameters();
                parameters.Add("p_id", campaignId, DbType.Int64);
                parameters.Add("p_state", campaignState, DbType.Int32);
                ExecuteScalar(Procedure.UpdateWhatsappCampaignState, parameters: parameters);

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex);
            }
        }


        public void UpdateMessage(int campaignId,string message, CampaignState campaignState)
        {
            try
            {

                var parameters = new DynamicParameters();
                parameters.Add("p_id", campaignId, DbType.Int64);
                parameters.Add("p_message", message);
                parameters.Add("p_campaign_state",(int)campaignState);
                ExecuteScalar(Procedure.UpdateWhatsappCampaignMessageResult, parameters: parameters);

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex);
            }
        }

        public void UpdateMessageCampaignState(int campaignId,  CampaignState campaignState)
        {
            try
            {

                var parameters = new DynamicParameters();
                parameters.Add("p_id", campaignId, DbType.Int64);
                parameters.Add("p_state", (int)campaignState);
                ExecuteScalar(Procedure.UpdateWhatsappCampaignState, parameters: parameters);

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex);
            }
        }
        public WhatsappAccount GetWhatsappAccount(int tenantId, int accountId, bool enableCaching = true)
        {
            if (enableCaching)
            {
                // string cacheKey = string.Format("{0}_{1}", tenantkey, channel);
                string cacheKey = string.Format(CacheKeys.WhatsappAccountKey, tenantId, accountId);


                return _cacheAdapter.Get(cacheKey, AppConstants.WhatsappAccount_Cache_Time_Minutes, () =>
                {
                    return GetWhatsappAccount(tenantId, accountId);
                });
            }
            else
            {
                return GetWhatsappAccount(tenantId, accountId);
            }
        }



        public void UpdateMessageTemplateDetailWaId(int messageTemplateDetailId, string WaId)
        {
            try
            {

                var parameters = new DynamicParameters();
                parameters.Add("p_id", messageTemplateDetailId, DbType.Int64);
                parameters.Add("p_wa_id", WaId);
                ExecuteScalar(Procedure.UpdateWhatsappCampaignMessageDetailWaIdResult, parameters: parameters);

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex);
            }
        }

        public void UpdateMessageTemplateDetailErrorResponse(int messageTemplateDetailId, string ErrorResponse)
        {
            try
            {

                var parameters = new DynamicParameters();
                parameters.Add("p_id", messageTemplateDetailId, DbType.Int64);
                parameters.Add("p_error_response", ErrorResponse);
                ExecuteScalar(Procedure.UpdateWhatsappCampaignMessageDetailErrorResponseResult, parameters: parameters);

            }
            catch (Exception ex)
            {
                _logAdapter.WriteLog(ex);
            }
        }

        #region Private Methods
        private WhatsappAccount GetWhatsappAccount(int tenantId, int accountId)
        {

            DynamicParameters dynamicParameters = new DynamicParameters();

            dynamicParameters.Add("_account_id", accountId);
            dynamicParameters.Add("_tenant_id", tenantId);
            WhatsappAccount whatsappAccounts = GetSingle<WhatsappAccount>(Procedure.GetWhatsappAccount, dynamicParameters);


            return whatsappAccounts;
        }

        public WhatsappAccount GetTemplateDetail(int accountId, int templateId)
        {
            throw new NotImplementedException();
        }

        #endregion Private Methods
    }
}
