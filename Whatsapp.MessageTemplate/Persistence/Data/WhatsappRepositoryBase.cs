﻿using Core.Common.Data;

namespace Whatsapp.MessageTemplate.Persistence.Data
{
    public abstract class WhatsappRepositoryBase<T> : DataRepositoryBase<T, WhatsappDbConnection>
    where T : class, new()
    {
        protected static object NullHandler(object instance)
        {
            if (instance != null)
                return instance;

            return DBNull.Value;
        }
    }
}
