﻿using Npgsql;
using System.Data;
using Whatsapp.MessageTemplate.Common.Configuration;

namespace Whatsapp.MessageTemplate.Persistence.Data
{
    public class WhatsappDbConnection : IDbConnection, IDisposable
    {
        public IDbConnection connection;

        public WhatsappDbConnection()
        {
            this.connection = new NpgsqlConnection(this.ConnectionString);
        }

        public string ConnectionString
        {
            get
            {

                return ApplicationConfigurationWhatsapp.WhatsappDbConnection;
            }
            set
            {
                connection.ConnectionString = value;
            }
        }

        public void Open()
        {
            connection.Open();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return connection.BeginTransaction(il);
        }

        public IDbTransaction BeginTransaction()
        {
            return connection.BeginTransaction();
        }

        public void ChangeDatabase(string databaseName)
        {
            connection.ChangeDatabase(databaseName);
        }

        public void Close()
        {
            connection.Close();
        }

        public int ConnectionTimeout
        {
            get { return connection.ConnectionTimeout; }
        }

        public IDbCommand CreateCommand()
        {
            return connection.CreateCommand();
        }

        public string Database
        {
            get { return connection.Database; }
        }

        public ConnectionState State
        {
            get { return connection.State; }
        }

        public void Dispose()
        {
            if (connection.State == ConnectionState.Open)
            {
                IDbCommand cmd = connection.CreateCommand();
                cmd.CommandText = "REVERT";
                cmd.ExecuteNonQuery();
            }

            connection.Dispose();
        }
    }
}
