﻿using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Persistence.Contracts.Operations
{
    public class WhatsappMessageOperation : BaseOperation<WhatsappMessage>
    {
        public int AccountId { get; set; }

        public string? DestinationAddress { get; set; }
    }
}
