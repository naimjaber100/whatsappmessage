﻿using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Persistence.Contracts.Repositories
{
    public interface ITemplateRepository
    {

        List<TemplateParameter> GetTemplateDetail(int accountId, string templateName);
    }
}
