﻿using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Client.Responses;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;

namespace Whatsapp.MessageTemplate.Persistence.Contracts.Repositories
{
    public interface IMessageRepository
    {
        int  Save(MessageRequestSave messageRequestSave, MessageResponseInfo messageResponseInfo);

        int SaveMessageReceived(WhatsappMessageReceived whatsappMessageReceived);

        int SaveMessageReceivedStatus(WhatsappMessageReceivedStatus whatsappMessageReceivedStatus);

        void Get(WhatsappMessageOperation whatsappMessageOperation);
        void GetByDestination(WhatsappMessageOperation whatsappMessageOperation);
    }
}
