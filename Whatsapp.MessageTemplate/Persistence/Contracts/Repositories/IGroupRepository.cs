﻿using Whatsapp.MessageTemplate.Entities;

namespace Whatsapp.MessageTemplate.Persistence.Contracts.Repositories
{
    public interface IGroupRepository
    {
        List<Contacts> GetGroupDetail(int groupId);
    }
}
