﻿using Monty.Omni.Framework.Entities;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Entities;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Persistence.Contracts.Repositories
{
    public interface IMessageTemplateRepository
    {
        void Get(WhatsappMessageTemplateOperation whatsappMessageTemplateOperation);
        int Save(MessageTemplateRequestSave messageTemplateRequestSave);
        int SaveDetail(ComponentRequestInfo componentRequestInfo, int campaignMessageId);
        WhatsappMessageTemplate GetById(int campaignId, int accountId);
        void AssignJob(int campaignId, string jobId);
        void ChangeStatus(int campaignId, CampaignState campaignState);

        void UpdateMessage(int campaignId, string message, CampaignState campaignState);

        void UpdateMessageCampaignState(int campaignId,  CampaignState campaignState);

        void UpdateMessageTemplateDetailWaId(int messageTemplateDetailId, string WaId);

        void UpdateMessageTemplateDetailErrorResponse(int messageTemplateDetailId, string ErrorResponse);
        List<WhatsappMessageTemplateDetail> GetWhatsappMessageTemplateDetailByCampaignId(int campaignId);

        WhatsappAccount GetWhatsappAccount(int tenantId, int accountId, bool enableCaching = true);


        WhatsappAccount GetTemplateDetail( int accountId, int templateId);

    }
}
