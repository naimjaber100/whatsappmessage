﻿using Common.Business.Common;
using Common.Business.Common.Engines;
using Common.Business.Services;
using Common.Client.Response;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.IdentityModel.Tokens.Jwt;
//using Microsoft.AspNetCore.SignalR;
using System.Net.WebSockets;
using System.Text;
using System.Web;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Business.Hubs;
using Whatsapp.MessageTemplate.Client.Requests;

namespace Whatsapp.MessageTemplate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : BaseApiController
    {

        private IMessageService _messageService;
       protected readonly Microsoft.AspNetCore.SignalR.IHubContext<ChatHub> _messageHub;
        //  protected readonly Microsoft.AspNetCore.SignalR.Hub _Hub;

        //  IHubContext<ChatHub, IChatHub> _chatHubContext;
        public MessageController(

           Microsoft.AspNetCore.SignalR.IHubContext<ChatHub> messageHub,

            ILogAdapter LogAdapter, IJsonAdapter JsonAdapter,
            IMessageService messageService) : base(LogAdapter, JsonAdapter)
        {
            _messageService = messageService;
             _messageHub = messageHub;

        }

        [HttpPost]
        //[Route("Message")]
        public ActionResult<ClientResponse> Save(MessageRequestSave messageRequestSave)
        {
          
            return ExecuteOperation(() =>
            {

                string tenantKey = IncomingRequestHeader.TenantKey;
                messageRequestSave.TenantId = Convert.ToInt32(tenantKey);

                messageRequestSave.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));



                return _messageService.Save(messageRequestSave);
            });
        }



        [HttpPost]
        [Route("MessageRead")]
        public ActionResult<ClientResponse> MessageRead(MessageMarkReadRequestSave messageMarkReadRequestSave)
        {

            return ExecuteOperation(() =>
            {

                string tenantKey = IncomingRequestHeader.TenantKey;
                messageMarkReadRequestSave.TenantId = Convert.ToInt32(tenantKey);

                messageMarkReadRequestSave.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));



                return _messageService.MessageRead(messageMarkReadRequestSave);
            });
        }

        //[HttpGet]
        //[Route("Webhook")]
        //public int Webhook([System.Web.Http.FromUri(Name = "hub.mode")] string mode, [System.Web.Http.FromUri(Name = "hub.challenge")] string challenge, [System.Web.Http.FromUri(Name = "hub.verify_token")] string verifyToken)
        //{


        //    if (mode == "subscribe" && verifyToken == "test123")
        //        //  return Content(challenge, "text/plain", Encoding.UTF8);

        //        return Convert.ToInt32(challenge);


        //    return 0;
        //    // return Content(string.Empty, "text/plain", Encoding.UTF8);

        //}


        [HttpGet]
        [Route("Webhook")]
        public int Webhook([FromQuery] Client.Requests.Hub hub)
        {

            Console.WriteLine(hub.mode);
            Console.WriteLine(hub.verify_token);
            Console.WriteLine(hub.challenge);
            Console.WriteLine(HttpContext.Request.QueryString.ToString());

            string queryParam = HttpContext.Request.QueryString.ToString();
            queryParam = queryParam.Replace("?", "");

            string[] queryParameter = queryParam.Split('&');
            string hubMode="";
            string verifyToken="";
            string Chalenge="";
            foreach (var query in queryParameter)
            {
                string[] querySplit = query.Split('=');
              
                if(querySplit[0] == "hub.mode")
                {
                    hubMode = querySplit[1].ToString();
                }

                if (querySplit[0] == "hub.challenge")
                {
                    Chalenge = querySplit[1].ToString();
                }

                if (querySplit[0] == "hub.verify_token")
                {
                    verifyToken = querySplit[1].ToString();
                }
                System.Console.WriteLine($"<{querySplit}>");
            }

            Console.WriteLine(hubMode);
            Console.WriteLine(verifyToken);
            Console.WriteLine(Chalenge);

            return Convert.ToInt32(Chalenge);


        }

        [HttpGet]
        [Route("WebhookOld")]
        public int WebhookOld([FromQuery] Client.Requests.Hub hub)
        {

            Console.WriteLine("test whatsapp");

            Console.WriteLine(hub.mode);
            Console.WriteLine(hub.verify_token);

            Console.WriteLine(hub.challenge);

            Console.WriteLine(HttpContext.Request.QueryString.ToString());

            string queryParam = HttpContext.Request.QueryString.ToString();
            //    string queryParam = "?hub.mode=subscribe&hub.challenge=1570011124&hub.verify_token=test123";
            queryParam = queryParam.Replace("?", "");

            // Uri myUri = new Uri(HttpContext.Request.QueryString.ToString());
            // string hubode = HttpUtility.ParseQueryString(myUri.Query).Get("hub.mode");

            string[] words = queryParam.Split('&');
            string hubMode = "";
            string verifyToken = "";
            string Chalenge = "";
            foreach (var word in words)
            {
                string[] words1 = word.Split('=');

                if (words1[0] == "hub.mode")
                {
                    hubMode = words1[1].ToString();
                }

                if (words1[0] == "hub.challenge")
                {
                    Chalenge = words1[1].ToString();
                }

                if (words1[0] == "hub.verify_token")
                {
                    verifyToken = words1[1].ToString();
                }
                System.Console.WriteLine($"<{word}>");
            }

            Console.WriteLine(hubMode);
            Console.WriteLine(verifyToken);
            Console.WriteLine(Chalenge);
            // Console.WriteLine(hubmode);
            //   if (hub.mode == "subscribe" && hub.verify_token == "test123")
            //  return Content(challenge, "text/plain", Encoding.UTF8);

            return Convert.ToInt32(Chalenge);


            //    return 0;
            // return Content(string.Empty, "text/plain", Encoding.UTF8);

        }

        [HttpPost]
        [Route("Webhook")]
        public ActionResult Webhooks([FromBody] object o)
        {

            Console.WriteLine(o);

            _messageService.Webhook(o);

           // test();

            return Ok();

        }


        [HttpPost]
        [Route("test")]
        public async Task<IActionResult> test()
        {
            //  await _chatHubContext.Clients.All.SendMessageId("ReceiveMessage", "The message");

         
            //   _messageHub.Clients.All.

         //   var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            ChatHub _connectedHub = new ChatHub();
            var connectedUserID = _connectedHub.GetConnectionId("5");
            // await _connectedHub.SendMessage("test");
            // await _connectedHub.SendMessageById("test", connectedUserID);
           await _messageHub.Clients.All.SendAsync("ReceiveMessage", "The message '" + "' has been received");
            await _messageHub.Clients.User(connectedUserID).SendAsync("ReceiveMessage", "The message user '" + "' has been received");
            //  await _messageHub.Clients.User(connectedUserID).SendMessage("ReceiveMessage","sdsf");

            return Ok();

        }


        [HttpGet]
        public ActionResult<ClientResponse> Get([FromQuery] MessageRequestInfo messageRequestInfo)
        {
            return ExecuteOperation(() =>
            {


                messageRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageService.Get(messageRequestInfo);
            });
        }

        [HttpGet]
        [Route("Destination")]
        public ActionResult<ClientResponse> GetByDestination([FromQuery] MessageRequestInfo messageRequestInfo)
        {
            return ExecuteOperation(() =>
            {


                messageRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageService.GetByDestination(messageRequestInfo);
            });
        }


    }
}
