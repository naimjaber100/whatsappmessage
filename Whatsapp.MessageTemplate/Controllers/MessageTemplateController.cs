﻿using Common.Business.Common;
using Common.Business.Services;
using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Client.Requests;
using GrapeCity.Documents.Excel;

namespace Whatsapp.MessageTemplate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageTemplateController : BaseApiController
    {

        private IMessageTemplateService _messageTemplateService;

        public MessageTemplateController(ILogAdapter LogAdapter, IJsonAdapter JsonAdapter, IMessageTemplateService messageTemplateService) : base(LogAdapter, JsonAdapter)
        {
            _messageTemplateService = messageTemplateService;
        }


        [HttpGet]
        public ActionResult<ClientResponse> Get([FromQuery] MessageTemplateRequestInfo messageTemplateRequestInfo )
        {
            return ExecuteOperation(() =>
            {

               
                messageTemplateRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageTemplateService.Get(messageTemplateRequestInfo);
            });
        }


        [HttpPost]
        public ActionResult<ClientResponse> SaveMessageTemplate([FromBody] MessageTemplateRequestSave messageTemplateRequestSave)
        {
            return ExecuteOperation(() =>
            {

                string tenantKey = IncomingRequestHeader.TenantKey;
                messageTemplateRequestSave.TenantId = Convert.ToInt32(tenantKey);

                messageTemplateRequestSave.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageTemplateService.Save(messageTemplateRequestSave);
            });
        }



        [HttpPost]
        [Route("ListMessageTemplate")]
        public ActionResult<ClientResponse> SaveListMessageTemplate([FromBody] MessageTemplateListRequestSave messageTemplateListRequestSave)
        {
            return ExecuteOperation(() =>
            {

                string tenantKey = IncomingRequestHeader.TenantKey;
                messageTemplateListRequestSave.TenantId = Convert.ToInt32(tenantKey);

                messageTemplateListRequestSave.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                        ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageTemplateService.SaveList(messageTemplateListRequestSave);
            });
        }



        [HttpPost]
        [Route("ValidListMessageTemplate")]
        public ActionResult<ClientResponse> ValidateListMessageTemplate([FromBody] List<ComponentRequestInfo> componentRequestInfo,[FromQuery] string templateName)
        {
            return ExecuteOperation(() =>
            {

              //  int accountId = 47;
                int accountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
             ?? throw new InvalidOperationException("Invalid AccountID"));

                return _messageTemplateService.ValidateList(componentRequestInfo, templateName, accountId);
            });
        }



        //[HttpPost]
        //[Route("GenerateExcelFile1")]
        //public FileResult ExportExcel(string fileName = "")
        //{
        //    // create a new Workbook and invoke FromJson to restore workbook from ssjson
        //    // the ssjson is from spread.sheets by invoking this.spread.toJSON()
        //    Workbook workbook = new Workbook();

        //    workbook.FromJson(HttpContext.Request.Body);
        //   // workbook.FromJson(HttpContext.Request.Body);

        //    MemoryStream stream = new MemoryStream();
        //    workbook.Save(stream);
        //    stream.Seek(0, SeekOrigin.Begin);

        //    byte[] bytes = new byte[stream.Length];
        //    stream.Read(bytes, 0, bytes.Length);

        //    var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    var donwloadFile = string.Format("attachment;filename={0}.xlsx;", string.IsNullOrEmpty(fileName) ? Guid.NewGuid().ToString() : WebUtility.UrlEncode(fileName));

        //    return File(bytes, contentType, donwloadFile);
        //}

        [HttpPost]
        [Route("GenerateExcelFile")]
        public IActionResult GenerateExcelFile(ExcelFileRequestInfo excelFileRequestInfo)
        {
            excelFileRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
            ?? throw new InvalidOperationException("Invalid AccountID"));

            return _messageTemplateService.GenerateTemplateExcelFile(excelFileRequestInfo);
           
        }



    }
}
