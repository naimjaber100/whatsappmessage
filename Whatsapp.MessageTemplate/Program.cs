using Com.Monty.Omni.Global.Business.Bootstrapper;
using Core.Common.Configurations;
using Hangfire;
using Hangfire.PostgreSql;
using Hangfire.Dashboard;
using Monty.Omni.Framework.Business.Contracts.Engines;
using Monty.Omni.Framework.Business.Engines;
using Whatsapp.MessageTemplate.Broker.Engine;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Business.Engine;
using Whatsapp.MessageTemplate.Business.Services;
using Whatsapp.MessageTemplate.Common.Configuration;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using Whatsapp.MessageTemplate.Persistence.Data.Repositories;
using Whatsapp.MessageTemplate.Business.Jobs;
using Microsoft.Extensions.DependencyInjection;
using Whatsapp.MessageTemplate.Business.Helpers;
using Whatsapp.MessageTemplate.Business.Hubs;
using Microsoft.AspNet.SignalR;
using Common.Business.Common.Engines;
using Common.Business.Engines;

var builder = WebApplication.CreateBuilder(args);

ConfigurationManager configuration = builder.Configuration; // allows both to access and to set up the config
IWebHostEnvironment environment = builder.Environment;

// Add services to the container.
builder.Services.AddCors();
builder.Services.AddControllers();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.InitOmni();

ApplicationConfiguration.DbConnection = Environment.GetEnvironmentVariable("DbConnection");
ApplicationConfiguration.DbConnectionSchemaName = Environment.GetEnvironmentVariable("DbConnectionSchemaName");
ApplicationConfiguration.ConfigurationDbConnection = Environment.GetEnvironmentVariable("ConfigurationDbConnection");
ApplicationConfiguration.ConfigurationDbConnectionSchemaName = Environment.GetEnvironmentVariable("ConfigurationSchemaName");
ApplicationConfigurationWhatsapp.WhatsappDbConnection = Environment.GetEnvironmentVariable("WhatsappDbConnection");
ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName = Environment.GetEnvironmentVariable("WhatsappSchemaName");


builder.Services.AddControllers().AddNewtonsoftJson();

builder.Services.AddSingleton<IMessageTemplateEngine, MessageTemplateEngine>();
builder.Services.AddSingleton<IMessageEngine, MessageEngine>();
builder.Services.AddSingleton<IMessageValidationEngine, MessageValidationEngine>();


builder.Services.AddSingleton<ChatHub>();

builder.Services.AddSingleton<IMessageTemplateService, MessageTemplateService>();
builder.Services.AddSingleton<IMessageService, MessageService>();

builder.Services.AddSingleton<IMessageTemplateRepository, MessageTemplateRepository>();
builder.Services.AddSingleton<ITemplateRepository, TemplateRepository>();
builder.Services.AddSingleton<IMessageRepository, MessageRepository>();

builder.Services.AddSingleton<IGroupRepository, GroupRepository>();
builder.Services.AddSingleton<IDateTimeEngine, DateTimeEngine>();

//builder.Services.AddSingleton<IHubContext,HubCon>();
builder.Services.AddTransient<ChatHub>();
builder.Services.AddHostedService<BrokerEngine>();

//builder.Services.ConfigureAuthentication();

builder.Services.ConfigureKeycloakAuthentication(environment);


builder.Services.AddHangfire((c => c.UsePostgreSqlStorage(Whatsapp.MessageTemplate.Common.Configuration.ApplicationConfigurationWhatsapp.WhatsappDbConnection, new PostgreSqlStorageOptions { SchemaName = "hangfire" })));
GlobalConfiguration.Configuration
            .UseActivator(new HangfireActivator(builder.Services.BuildServiceProvider()));

builder.Services.AddHangfireServer();
builder.Services.AddSignalR();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();
app.UseCors(options =>
{
    options.AllowAnyMethod()
            .AllowAnyOrigin()
            .AllowAnyHeader();
});

app.UseWebSockets();
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

//app.MapControllers();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHub<ChatHub>("/ChatHub");
});


app.Run();


