﻿using static Com.Monty.Omni.Global.Common.Enums.Enumerations;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageTemplate1
    {
        public int CampaignId { get; set; }
        public string? CampaignName { get; set; } 
        public int AccountId { get; set; }
        public DateTime CreatedDate { get; set; } 
        public DateTime ScheduleDate { get; set; }
        public CampaignState State { get; set; }
        public string? TimeZoneId { get; set; }
        public string? MessagingProduct { get; set; }
        public string? RecipientType { get; set; }
        public string? TemplateName { get; set; }
        public string? Language { get; set; }
        public int TenantId { get; set; }
    }

 
}
