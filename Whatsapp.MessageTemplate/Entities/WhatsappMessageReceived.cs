﻿using Newtonsoft.Json;
using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageReceived
    {
        public string? DisplayPhoneNumber { get; set; }
        public string? PhoneNumberId { get; set; }
        public string? WaId { get; set; }
        public string? ProfileName { get; set; }
        public string? FromNumber { get; set; }
        public string? MessageId { get; set; }
        public string? Timestamp { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public MessageType? Type { get; set; }
        public string? Body { get; set; }
        public string? Caption { get; set; }
        public string? FileName { get; set; }
        public string? MimeType { get; set; }
        public string? Sha256 { get; set; }
        public string? MediaId { get; set; }
        public string? Field { get; set; }
        public bool? Voice { get; set; }


    }

    public class objects
    {
        public string? dataobject { get; set; }

        public List<entry>? entry { get; set; }
    }

    public class entry
    {
        public string? id { get; set; }
        public List<changes>? changes { get; set; }
    }

    public class changes
    {
        public string? field { get; set; }
        public value? value { get; set; }
    }

    public class value
    {
        public string? messaging_product { get; set; }
        public metadata? metadata { get; set; }
        public List<contacts>? contacts { get; set; }
        public List<messages>? messages { get; set; }

        public List<statuses>? statuses { get; set; }
    }

    public class metadata
    {
        public string? display_phone_number { get; set; }
        public string? phone_number_id { get; set; }
    }

    public class contacts
    {
        public profile? profile { get; set; }
        public string? wa_id { get; set; }
    }

    public class profile
    {
        public string? name { get; set; }
    }

    public class messages
    {
        public string? from { get; set; }
        public string? id { get; set; }
        public string? timestamp { get; set; }
        public MessageType? type { get; set; }
        public text? text { get; set; }

        public video? video { get; set; }

        public document? document { get; set; }

        public image? image { get; set; }
        public sticker? sticker { get; set; }
        public audio? audio { get; set; }
    }

    public class text
    {
        public string? body { get; set; }
    }

    public class video
    {
        public string? caption { get; set; }
        public string? mime_type { get; set; }
        public string? sha256 { get; set; }
        public string? id { get; set; }
        public string? field { get; set; }
    }

    public class document
    {
        public string? caption { get; set; }
        public string? filename { get; set; }
        public string? mime_type { get; set; }
        public string? sha256 { get; set; }
        public string? id { get; set; }
    }

    public class image
    {
        public string? caption { get; set; }
        public string? mime_type { get; set; }
        public string? sha256 { get; set; }
        public string? id { get; set; }
    }

    public class sticker
    {
        public string? mime_type { get; set; }
        public string? sha256 { get; set; }
        public string? id { get; set; }
    }

    public class audio
    {

        public string? mime_type { get; set; }
        public string? sha256 { get; set; }
        public string? id { get; set; }
        public bool voice { get; set; }
    }

    public class statuses
    {
        [JsonProperty("id")]
        public string? Id { get; set; }
        [JsonProperty("status")]
        public string? Status { get; set; }
        [JsonProperty("timestamp")]
        public string? Timestamp { get; set; }
        [JsonProperty("recipient_id")]
        public string? RecipientId { get; set; }
    }
}
