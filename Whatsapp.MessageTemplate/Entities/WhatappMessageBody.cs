﻿using Whatsapp.MessageTemplate.Client.Requests;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatappMessageBodyText: WhatsappMessageBodyRequestInfo
    {

        public object? text { get; set; }

        public bool? preview_url { get; set; }
        //  public object? image { get; set; }
    }

    public class WhatappMessageBodyImage : WhatsappMessageBodyRequestInfo
    {

        public media? image { get; set; }
    }

    public class WhatappMessageBodyDocument : WhatsappMessageBodyRequestInfo
    {

        public media? document { get; set; }
    }

    public class WhatappMessageBodyVideo : WhatsappMessageBodyRequestInfo
    {

        public media? video { get; set; }
    }


    public class media
    {
        public string? link { get; set; }

        public string? caption { get; set; } 
    }
}
