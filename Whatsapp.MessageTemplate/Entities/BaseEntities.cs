﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Whatsapp.MessageTemplate.Entities
{
    public class BaseEntities
    {
        public string GetKeyFieldName()
        {
            string key = "";

            IEnumerable<PropertyInfo> props = this.GetType().GetProperties().Where(
                                                p => Attribute.IsDefined(p, typeof(OmniKeyAttribute)));

            //var name = item.GetCustomAttribute<ColumnAttribute>().Name ?? item.Name;
            key = String.Join(",", props.Select(x => x.GetCustomAttribute<ColumnAttribute>().Name ?? x.Name).ToArray());

            return key;
        }
        public IEnumerable<PropertyInfo> GetFeildsPropertyInfos()
        {

            IEnumerable<PropertyInfo> props = this.GetType().GetProperties().Where(
                prop => Attribute.IsDefined(prop, typeof(ColumnAttribute)));

            return props;
        }
        public IEnumerable<PropertyInfo> GetFeildsPropertyValue()
        {

            IEnumerable<PropertyInfo> props = this.GetType().GetProperties();

            return props;
        }


        public class OmniKeyAttribute : Attribute
        {
            public OmniKeyAttribute()
            {
            }

        }
    }
}
