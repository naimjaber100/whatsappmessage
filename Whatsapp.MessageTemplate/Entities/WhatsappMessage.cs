﻿using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessage
    {

        public string? DestinationAddress { get; set; }
        public string? MessageState { get; set; }
        public string? MessageText { get; set; }
        public string? MessageId { get; set; }
        public string? Link { get; set; }
        public WhatsappMessageStatus? Status { get; set; }
        public DateTime CreatedDate { get;set; }

        public MessageType Type { get; set; }
        public string TypeString {
            get
            {
                return Type.ToString();
            }
        }
        public int TotalRows { get; set; }


    }
}
