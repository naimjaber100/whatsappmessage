﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class Contacts
    {
        public int ContactId { get; set; }

        public string? FullName { get; set; }

        public string? MobileNo { get; set; }
    }
}
