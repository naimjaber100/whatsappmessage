﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class TemplateParameter
    {
    public int id { get; set; }
    public DateTime createdDate { get; set; }
    public string? typeRequest { get; set; }
    public string? format { get; set; } 
    public string? text { get; set; }
    public string? buttonType { get; set; }
    public string? url { get; set; }
    public string? phoneNumber { get; set; }
    public int templateId { get; set; }
    public string? example { get; set; }
    public int numberOfVariable { get; set; }
}
}
