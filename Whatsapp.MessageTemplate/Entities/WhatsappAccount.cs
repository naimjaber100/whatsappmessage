﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappAccount
    {
        public int Id { get; set; }
        public string? WhatsappAccountBusinessId { get; set; }
        public string? Token { get; set; }
        public int OmniAccountId { get; set; }
        public string? PhoneNumberId { get; set; }
    }
}
