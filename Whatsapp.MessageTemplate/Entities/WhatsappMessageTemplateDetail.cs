﻿using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageTemplateDetail
    {
        public int Id { get; set; }
        public int CampaignMessageTemplateId { get;set; }
        public string? TypeHeader { get; set; }
        public string? MediaType { get; set; }
        public string? Link { get; set; }
        public string? FileName { get; set; }
        public string? HeaderName { get; set; }
        public string? TypeBody { get; set; }    
        public string? BodyText { get; set; }
        public string? DestinationAddress { get; set; }
        public string? HeaderText { get; set; }
    }
}
