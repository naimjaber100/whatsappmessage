﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageTemplateBody
    {
        public string? messaging_product { get; set; }
        public string? recipient_type { get; set; }
        public string? to { get;set; }  
        public string? type { get; set; }
        public Template? template { get; set; }
    }

    public class Template
    {
        public string? name { get; set; }
        public Language? language { get; set; }

        public List<object>? components { get; set; }
    }

    public class Language
    {
        public string? code { get; set; }
    }

    public class Body
    {
        public string? type { get; set; }
        public List<ParameterBody>? parameters { get; set; }
    }

    public class HeaderText
    {
        public string? type { get; set; }
        public List<ParameterHeaderText>? parameters { get; set; }
    }

    public class HeaderMedia
    {
        public string? type { get; set; }
        public List<object>? parameters { get; set; }
    }
    public class ParameterBody
    {
        public string? type { get; set; }
        public string? text { get; set; }

    }

    public class ParameterHeaderImageMedia
    {
        public string? type { get; set; }
        public Media? image { get; set; }

    }

    public class ParameterHeaderVideoMedia
    {
        public string? type { get; set; }
        public Media? video { get; set; }

    }

    public class ParameterHeaderDocumentMedia
    {
        public string? type { get; set; }
        public Media? document { get; set; }

    }
    public class ParameterHeaderText
    {
        public string? type { get; set; }
        public string? text { get; set; }

    }

    public class Media
    {
        public string? link { get; set; }
    }

  
}
