﻿
using static Whatsapp.MessageTemplate.Common.Enums.Enums;

namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageReceivedStatus
    {

        public string? WaId { get; set; }
        public WhatsappMessageStatus? Status { get; set; }
        public string? RecipientId { get; set; }
    }
}
