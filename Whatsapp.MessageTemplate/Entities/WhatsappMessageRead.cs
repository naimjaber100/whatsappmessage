﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappMessageRead
    {

        public string? MessagingProduct { get; set; }
        public string? Status { get; set; }
        public string? MessageId { get; set; }
    }
}
