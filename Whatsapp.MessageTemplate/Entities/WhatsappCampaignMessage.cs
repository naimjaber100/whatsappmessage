﻿namespace Whatsapp.MessageTemplate.Entities
{
    public class WhatsappCampaignMessage
    {

        public int campaign_id { get; set; }
        public string? MessagingProduct { get; set; } 
        public string? RecipientType { get; set; }
        public string? TemplateName { get; set; }
        public string? Language { get; set; }
        public string? FromPhoneNumber { get; set; }
        public string? CreatedDate { get; set; }
        public string? ScheduleDate { get; set; }
        public int AccountId { get; set; }
        public string? State { get; set; }
        public string? TimezoneId { get; set; }
        public string? TenantId { get; set; }
        public string? Type { get; set; }
        public string? CampaignName { get; set; }
        public string? JobId { get; set; }
        public string? MessageResponse { get; set; }

    }

    public class WhatsappCampaignMessageSummary : WhatsappCampaignMessage
    {
        public int TotalRows { get; set; }
    }
}
