﻿using Com.Monty.Omni.Global.Broker.Engines;
using Com.Monty.Omni.Global.Business.Bootstrapper;
using Microsoft.Extensions.DependencyInjection;
using Monty.Omni.Framework.Business.Contracts.Engines;
using Monty.Omni.Framework.Business.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Business.Engine;
using Whatsapp.MessageTemplate.Business.Helpers;
using Whatsapp.MessageTemplate.Business.Hubs;
using Whatsapp.MessageTemplate.Business.Services;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using Whatsapp.MessageTemplate.Persistence.Data.Repositories;

namespace WhatsappMessageTemplate.XUnitTest
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            Environment.SetEnvironmentVariable("externalId", "5");
            Environment.SetEnvironmentVariable("TenantKey", "9ce64459-25d0-4671-a287-8626c4661905");
            Environment.SetEnvironmentVariable("LoginSignature", "fnvaN2S43O7wcQk218XOlhOpMEFV7a/OZc617BNbK4DDfyzFOiqS8AhpsNWAETKMvUIdfm2Dk5oXuKCT4OiDIA==");
            Environment.SetEnvironmentVariable("LoginTokenUrl", "api/v1/Auth/Login");
            Environment.SetEnvironmentVariable("RegisterUrl", "api/v1/Users/Register");
            Environment.SetEnvironmentVariable("IdentityUrl", "api/v1/Users");
            Environment.SetEnvironmentVariable("IdentityBaseUrl", "https://identity-dev.montylocal.net/");
            Environment.SetEnvironmentVariable("TenantSignature", "mhl8djytIggNCFKxjdUizwHKRDjq6V7wT051J1wIuSwjq5swQZH2TJzf/SSkmgTiKzIbSjRcGeMmSSdrnPX0PQ==");
            Environment.SetEnvironmentVariable("GetTenantUrl", "api/v2/Tenants");
            Environment.SetEnvironmentVariable("RolesUrl", "api/v1/Roles");
            Environment.SetEnvironmentVariable("ToggleUrl", "api/v1/Users/ToggleActivation");
            Environment.SetEnvironmentVariable("LoginTokenUrlV2", "api/v2/Auth/Token");
            Environment.SetEnvironmentVariable("DbConnection", "Username=postgres;Password=Z8j7KqgzrTTS9RqY;Server=139.162.181.135;Port=5432;Database=omnidb;");
            Environment.SetEnvironmentVariable("ConfigurationDbConnection", "Username=postgres;Password=Z8j7KqgzrTTS9RqY;Server=139.162.181.135;Port=5432;Database=omni_config_external;");
            Environment.SetEnvironmentVariable("BalanceManagerDbConnection", "Username=postgres;Password=fMJiuFtB4#b@;Server=134.119.218.194;Port=5432;Database=omnidb;");

            services.AddSingleton<IMessageTemplateEngine, MessageTemplateEngine>();
            services.AddSingleton<IMessageEngine, MessageEngine>();
            services.AddSingleton<IMessageValidationEngine, MessageValidationEngine>();


            services.AddSingleton<ChatHub>();

            services.AddSingleton<IMessageTemplateService, MessageTemplateService>();
            services.AddSingleton<IMessageService, MessageService>();

            services.AddSingleton<IMessageTemplateRepository, MessageTemplateRepository>();
            services.AddSingleton<ITemplateRepository, TemplateRepository>();
            services.AddSingleton<IMessageRepository, MessageRepository>();

            services.AddSingleton<IGroupRepository, GroupRepository>();
            services.AddSingleton<IDateTimeEngine, DateTimeEngine>();
            services.AddHostedService<BrokerEngine>();

            services.ConfigureAuthentication();

        }
    }
}
