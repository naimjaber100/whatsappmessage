﻿using Com.Monty.Omni.Global.Broker.Contracts.Engines;
using Common.Business.Common;
using Common.Client.Response;
using Common.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsapp.MessageTemplate.Business.Contracts.Engines;
using Whatsapp.MessageTemplate.Business.Contracts.Services;
using Whatsapp.MessageTemplate.Business.Engine;
using Whatsapp.MessageTemplate.Business.Services;
using Whatsapp.MessageTemplate.Client.Requests;
using Whatsapp.MessageTemplate.Controllers;
using Whatsapp.MessageTemplate.Persistence.Contracts.Operations;
using Whatsapp.MessageTemplate.Persistence.Contracts.Repositories;
using Whatsapp.MessageTemplate.Persistence.Data.Repositories;
using WhatsappMessageTemplate.XUnitTest.Extension;
using Xunit;

namespace WhatsappMessageTemplate.XUnitTest.ControllerTest
{
    public class MessageTemplateControllerTest
    {
        #region Fields

        private readonly MessageTemplateController _Controller;
        private readonly Mock<IMessageTemplateService> _mockService;
        private readonly Mock<IMessageTemplateEngine> _mockEngine;
        private readonly Mock<ILogAdapter> _logAdapter;
        private readonly Mock<IJsonAdapter> _jsonAdapter;
        private readonly Mock<ICacheAdapter> _cacheAdapter;
        // private readonly AccountReportHelper _accountHelper;



        private readonly string _externalId = "5";

        #endregion


        #region Contructors
        public MessageTemplateControllerTest()
        {
            _mockService = new Mock<IMessageTemplateService>();
           _mockEngine = new Mock<IMessageTemplateEngine>();
            _logAdapter = new Mock<ILogAdapter>();
            _jsonAdapter = new Mock<IJsonAdapter>();
            _cacheAdapter = new Mock<ICacheAdapter>();

            var services = new ServiceCollection();
            services.AddSingleton<IMessageTemplateService, MessageTemplateService>();
            services.AddSingleton<IMessageTemplateEngine, MessageTemplateEngine>();
            services.AddSingleton<IMessageTemplateRepository, MessageTemplateRepository>();

            var serviceProvider = services.BuildServiceProvider();
            _Controller = new MessageTemplateController( _logAdapter.Object, _jsonAdapter.Object, _mockService.Object).WithIdentity(_externalId);
        }
        #endregion


        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {

            var mockinfo = new Mock<MessageTemplateRequestInfo>();

            MessageTemplateRequestInfo messageTemplateRequestInfo = new MessageTemplateRequestInfo()
            {
                CampaignName = "test",
                PageIndex = 1,
                PageSize = 25
            };

            _mockService.Setup(m => m.Get(mockinfo.Object))
              .Returns(new ClientResponse { Status = "success", Data = messageTemplateRequestInfo });

 
            var Result = _Controller.Get(mockinfo.Object);
            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(Result);

            Assert.IsType<ActionResult<ClientResponse>>(Result);

           Assert.NotNull(okResult.Value);
           Assert.NotNull(okResult.Value?.Data);
           Assert.True(okResult.Value?.Status == "success");
           _mockService.Verify(c => c.Get(mockinfo.Object), Times.Once);
        }

        [Fact]
        public void Add_ValidObjectPassed_ReturnsCreatedResponse()
        {

            var mockinfo = new Mock<MessageTemplateRequestSave>();
            // Arrange
            _mockService.Setup(m => m.Save(mockinfo.Object))
             .Returns(new ClientResponse { Status = "success", ResponseCode = System.Net.HttpStatusCode.OK });

            // Act
            var createdResponse = _Controller.SaveMessageTemplate(mockinfo.Object);
            //Assert
            var okResult = Assert.IsType<ActionResult<ClientResponse>>(createdResponse);

            AssertWithSuccess(okResult);

            _mockService.Verify(c => c.Save(mockinfo.Object), Times.Once);
        }


        #region AssertWithSuccess
        internal void AssertWithSuccess(ActionResult<ClientResponse> result)
        {
            Assert.NotNull(result.Value);
            Assert.NotNull(result?.Value?.ResponseCode);

            Assert.Equal("success", result?.Value?.Status);
            Assert.Equal(System.Net.HttpStatusCode.OK, result?.Value?.ResponseCode);
        }
        #endregion
    }
}
